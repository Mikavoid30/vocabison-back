import dotenv from 'dotenv'
import Joi from 'joi'
import fs from 'fs'

export interface EnvConfig {
  [key: string]: string
}
export class ConfigService {
  private readonly envConfig: EnvConfig

  constructor(filePath: string) {
    const config = dotenv.parse(fs.readFileSync(filePath))
    this.envConfig = this.validateInput(config)
  }

  get(key: string): string {
    return this.envConfig[key]
  }

  private validateInput(config: EnvConfig) {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['dev', 'test', 'production'])
        .default('dev'),
      PORT: Joi.number().default(4000),
      SECRET: Joi.string().required(),
      SESSION_SECRET: Joi.string().required(),
      ADMIN_EMAIL: Joi.string().default('mikavoid@gmail.com'),
      APP_URL: Joi.string().default('http:/localhost:3000'),
      PASSWORD_MIN_LENGTH: Joi.number().default(6),
      DATABASE_USER: Joi.string().default('postgres'),
      DATABASE_PASSWORD: Joi.string().default(''),
      DATABASE_NAME: Joi.string().default('postgres'),
      DATABASE_TEST: Joi.string().default('testdb'),
      GOOGLE_APPLICATION_CREDENTIALS: Joi.string().default('/home'),
      DO_FAKE_SCRAPING: Joi.boolean().default(false),
      DO_FAKE_TRANSLATIONS: Joi.boolean().default(false),
      LEXICALA_URL: Joi.string().default(''),
      LEXICALA_USERNAME: Joi.string().default('vocabison'),
      LEXICALA_PASSWORD: Joi.string().default('secret')
    })

    const { error, value: validatedEnvConfig } = Joi.validate(
      config,
      envVarsSchema
    )

    if (error) {
      throw new Error(`Config validation error: ${error.message}`)
    }

    return validatedEnvConfig
  }
}
