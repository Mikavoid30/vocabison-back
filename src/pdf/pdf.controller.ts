import { Controller, Res, Get, Param } from '@nestjs/common'
import { PdfService } from './pdf.service'
import { Response } from 'express'

@Controller('pdf')
export class PdfController {
  constructor(private pdfService: PdfService) {}

  @Get(':token')
  async pdf(@Param('token') token: string, @Res() res: Response) {
    /** @todo jwt auth */
    // if (req.session.userId !== userId)
    //   res.send({ error: 'Unauthorized', path: null })
    try {
      const filePath =
        (await this.pdfService.generatePdf(token, {
          colors: {},
          sizes: {}
        })) || ''
      const splitted = filePath.split('.')
      let ext = splitted.length ? splitted[splitted.length - 1] : ''
      setTimeout(() => {
        try {
          return res.download(
            filePath || '',
            `vocabison-export-${new Date().toLocaleDateString()}.${ext}`
          )
        } catch (e) {
          return res.status(404).send()
        }
      }, 1000)
    } catch (e) {
      return res.status(404).send()
    }
  }
}
