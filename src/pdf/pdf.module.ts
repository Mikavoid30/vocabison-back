import { Module } from '@nestjs/common'
import { PdfController } from './pdf.controller'
import { PdfService } from './pdf.service'
import { FileManagerModule } from '../file-manager/file-manager.module'

@Module({
  imports: [FileManagerModule],
  providers: [PdfService],
  controllers: [PdfController],
  exports: [PdfService]
})
export class PdfModule {}
