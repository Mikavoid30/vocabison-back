import PdfPrinter from 'pdfmake'
import { Injectable } from '@nestjs/common'
import { FileManagerService } from '../file-manager/file-manager.service'

type Theme = {
  colors: {
    borders?: string
    grey?: string
    dark?: string
    primary?: string
    secondary?: string
    synonyms?: string
    examples?: string
    definitions?: string
  }
  sizes: {
    strates?: number
  }
}

type Strate = {
  name: string
  horizontal?: boolean
  style?: {
    fontSize?: number
    color?: string
  }
}

const BisonFileKeys = {
  word: 'w',
  type: 't',
  otherTranslations: 'o',
  lang: 'l',
  comment: 'c',
  synonyms: 's',
  examples: 'e',
  definitions: 'd'
}

@Injectable()
export class PdfService {
  _printer: any = null
  _theme: Theme = null

  constructor(private fileManagerService: FileManagerService) {
    this._theme = { colors: {}, sizes: {} }
    this._initPrinter()
  }

  async generatePdf(
    bisonFileToken: string = '',
    theme: Theme = { colors: {}, sizes: {} }
  ) {
    if (!bisonFileToken) return
    this._theme = this._buildTheme(theme)
    const data = await this.fileManagerService.getBisonData(bisonFileToken)
    const dd = this._buildPdfData(data)
    const pdfDoc = await this._printer.createPdfKitDocument(dd, {})

    // Will save a temp file and delete it after 10s
    return await this.fileManagerService.saveAutoDeleteFile(
      bisonFileToken + '.pdf',
      pdfDoc
    )
  }

  /**
   * Build an entire cell containing expression, synonyms etc...
   * @param data
   * @param strates // synonyms etc...
   * @param layout  // Layout of the inner table
   */
  _buildExpressionCell(data: object, strates: Strate[], layout = 'noBorders') {
    const lang = data[BisonFileKeys.lang] || 'en'
    const cell = {
      layout,
      table: {
        widths: '100%',
        body: []
      }
    }
    // Add main expression
    cell.table.body.push([
      this._buildTextCell(data[BisonFileKeys.word], lang, {
        bold: true,
        alignment: 'left'
      })
    ])

    // Add optionals
    const lgt = strates.length
    for (let i = 0; i < lgt; i++) {
      const currentStrate = strates[i]
      const words = data[BisonFileKeys[currentStrate.name]]
      const style = currentStrate.style || {}
      if (!words.length) continue

      cell.table.body.push([
        currentStrate.horizontal
          ? words.map((w: string) => [this._buildTextCell(w, lang, style)])
          : [this._buildTextCell(words.join(', '), lang, style)]
      ])
    }

    return cell
  }

  _buildTextCell(text: string, lang = 'en', style = {}) {
    return { ...style, style: lang.toLowerCase(), text }
  }

  _buildPdfData(data: any) {
    const { meta = {}, entries = [] } = data
    const nbLanguages = meta.nbLangs || 1
    const strates: Strate[] = [
      {
        name: 'definitions',
        horizontal: false,
        style: {
          color: this._theme.colors.definitions,
          fontSize: this._theme.sizes.strates
        }
      },
      {
        name: 'examples',
        horizontal: false,
        style: {
          color: this._theme.colors.examples,
          fontSize: this._theme.sizes.strates
        }
      },
      {
        name: 'synonyms',
        horizontal: true,
        style: {
          color: this._theme.colors.synonyms,
          fontSize: this._theme.sizes.strates
        }
      }
    ]

    return {
      footer: (currentPage: number, pageCount: any) => {
        return {
          layout: 'noBorders',
          table: {
            widths: ['50%', '50%'],
            headerRows: 0,
            body: [
              [
                {
                  text: 'Powered by vocabison.com (ᵔᴥᵔ)',
                  color: this._theme.colors.grey || 'black',
                  alignment: 'left',
                  margin: [40, 0, 0, 0],
                  fontSize: 10,
                  font: 'NotoSans'
                },
                {
                  text: currentPage.toString() + ' / ' + pageCount,
                  alignment: 'right',
                  margin: [0, 0, 40, 0],
                  color: this._theme.colors.primary || '#ccc'
                }
              ]
            ]
          }
        }
      },
      content: [
        {
          layout: 'noBorders',
          style: 'header',
          table: {
            widths: ['80%', '121px'],
            headerRows: 0,
            body: [
              [
                [
                  { text: meta.title || '', style: 'subheader' },
                  {
                    text: meta.tagline,
                    style: 'languages'
                  }
                ],
                [
                  {
                    margin: [0, 10, 0, 10],
                    rowSpan: 2,
                    image: __dirname + '/logo.png',
                    width: 120,
                    alignment: 'right'
                  }
                ]
              ]
            ]
          }
        },
        {
          style: 'mainTable',
          layout: {
            hLineColor: this._theme.colors.borders || 'gray',
            vLineColor: this._theme.colors.borders || 'gray',
            fillColor: (rowIndex: number) => {
              return rowIndex % 2 === 0 ? '#fafafa' : null
            }
          },
          table: {
            headerRows: 1,
            widths: Array(nbLanguages).fill(
              Math.floor(100 / nbLanguages) + '%'
            ),
            body: [
              meta.langs.map((x: any) => ({
                text: x.text,
                style: 'tableHeader'
              })),
              ...entries.map((entry: any) => {
                return meta.langs.map((o: any) => {
                  const validExp = entry.exps.find(
                    (e: any) => e.l.toLowerCase() === o.code.toLowerCase()
                  )
                  if (!validExp) return '---'
                  return this._buildExpressionCell(validExp, strates)
                })
              })
            ]
          }
        }
      ],
      styles: {
        header: {
          margin: [0, 0, 20, 0]
        },
        subheader: {
          fontSize: 16,
          color: this._theme.colors.primary,
          bold: true,
          margin: [5, 10, 10, 0]
        },
        languages: {
          bold: false,
          fontSize: 12,
          color: this._theme.colors.secondary || '#f3345d',
          margin: [5, 5, 10, 10]
        },
        mainTable: {
          margin: [0, 15, 0, 0],
          color: this._theme.colors.dark || '#333333',
          fontSize: 11
        },
        tableHeader: {
          bold: true,
          fontSize: 11,
          fillColor: this._theme.colors.primary || '#207e8f',
          color: 'white',
          margin: [5, 5, 5, 5]
        },
        custom: {
          fillColor: 'black'
        },
        ar: { font: 'Tajawal' }, // Arabic
        zh: { font: 'NotoSansSC' }, // Chinese
        da: { font: 'NotoSans' }, // Danish
        nl: { font: 'NotoSans' }, // Dutch
        en: { font: 'NotoSans' }, // English
        fr: { font: 'NotoSans' }, // French
        de: { font: 'NotoSans' }, // German
        el: { font: 'GFS_Neohellenic' }, // Greek
        hu: { font: 'NotoSans' }, // Hungarian
        it: { font: 'NotoSans' }, // Italian
        ja: { font: 'NotoSansJP' }, // Japanese
        ko: { font: 'NotoSansKR' }, // Korean
        lt: { font: 'NotoSans' }, // Lithuanian
        pl: { font: 'NotoSans' }, // Polish
        pt: { font: 'NotoSans' }, // Portugese
        ru: { font: 'NotoSansSC' }, // Russian
        es: { font: 'NotoSans' }, // Spanish
        sv: { font: 'NotoSans' }, // Swedish
        tr: { font: 'NotoSans' }, // Turkish
        vi: { font: 'NotoSans' } // Vietnamese
      }
    }
  }

  _buildTheme(theme: Theme): Theme {
    return {
      colors: {
        borders: '#cccccc',
        primary: '#216479',
        secondary: '#F3345D',
        synonyms: '#D93A3A',
        examples: '#279097',
        definitions: '#989999',
        grey: '#989999',
        dark: '#464646',
        ...theme.colors
      },
      sizes: {
        strates: 8,
        ...theme.sizes
      }
    }
  }

  _initPrinter() {
    this._printer = new PdfPrinter({
      Roboto: {
        normal: __dirname + '/fonts/Roboto-Regular.ttf',
        bold: __dirname + '/fonts/Roboto-Bold.ttf',
        italics: __dirname + '/fonts/Roboto-Italic.ttf',
        bolditalics: __dirname + '/fonts/Roboto-BoldItalic.ttf'
      },
      NotoSans: {
        normal: __dirname + '/fonts/NotoSans-Regular.ttf',
        bold: __dirname + '/fonts/NotoSans-Bold.ttf',
        bolditalics: __dirname + '/fonts/NotoSans-BoldItalic.ttf',
        italics: __dirname + '/fonts/NotoSans-Italic.ttf'
      },
      NotoSansSC: {
        normal: __dirname + '/fonts/NotoSansSC-Regular.otf',
        bold: __dirname + '/fonts/NotoSansSC-Bold.otf',
        light: __dirname + '/fonts/NotoSansSC-Light.otf',
        black: __dirname + '/fonts/NotoSansSC-Black.otf'
      },
      NotoSansJP: {
        normal: __dirname + '/fonts/NotoSansJP-Regular.otf',
        bold: __dirname + '/fonts/NotoSansJP-Bold.otf',
        light: __dirname + '/fonts/NotoSansJP-Light.otf',
        medium: __dirname + '/fonts/NotoSansJP-Medium.otf'
      },
      NotoSansKR: {
        normal: __dirname + '/fonts/NotoSansKR-Regular.otf',
        bold: __dirname + '/fonts/NotoSansKR-Bold.otf',
        light: __dirname + '/fonts/NotoSansKR-Light.otf',
        medium: __dirname + '/fonts/NotoSansKR-Medium.otf'
      },
      GFS_Neohellenic: {
        normal: __dirname + '/fonts/GFSNeohellenic-Regular.ttf',
        bold: __dirname + '/fonts/GFSNeohellenic-Bold.ttf',
        italics: __dirname + '/fonts/GFSNeohellenic-Italic.ttf',
        bolditalics: __dirname + '/fonts/GFSNeohellenic-BoldItalic.ttf'
      },
      Tajawal: {
        normal: __dirname + '/fonts/Tajawal-Medium.ttf',
        bold: __dirname + '/fonts/Tajawal-Bold.ttf',
        light: __dirname + '/fonts/Tajawal-Light.ttf',
        black: __dirname + '/fonts/Tajawal-ExtraBold.ttf'
      }
    })
  }
}
