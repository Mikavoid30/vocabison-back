import { Injectable, NotFoundException } from '@nestjs/common'
import { BoxEntity } from '../box/box.entity'
import { EntryEntity } from './entry.entity'
import { Repository, Like } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'
import { UserEntity } from '../user/user.entity'
import { CreateEntryDTO } from './dto/create-entry.dto'
import { UpdateEntryDTO } from './dto/update-entry.dto'
import { PaginationInputDTO } from './dto/query-entries.dto'
import { SearchInputDTO } from './dto/search-input.dto'

@Injectable()
export class EntryService {
  constructor(
    @InjectRepository(EntryEntity)
    private entryRepository: Repository<EntryEntity>,
    @InjectRepository(BoxEntity) private boxRepository: Repository<BoxEntity>,
    @InjectRepository(UserEntity) private userRepository: Repository<UserEntity>
  ) {}

  async findAllFromBox(
    userId: string,
    boxId: string,
    { take, from }: PaginationInputDTO,
    { term }: SearchInputDTO = { term: '' }
  ): Promise<EntryEntity[]> {
    const owner = await this.get('user', userId)
    const box = await this.boxRepository.findOneOrFail({
      where: { id: boxId, owner }
    })

    let entries = await this.entryRepository.find({
      relations: ['expressions', 'expressions.strates'],
      where: [{ box: box.id, expressions: true }],
      order: {
        created: 'DESC'
      },
      skip: from,
      take
    })

    // Search filters
    if (term && term.length) {
      const rexp = new RegExp(term)
      entries = entries.filter((x) =>
        x.expressions.some((x) => rexp.test(x.text))
      )
    }

    return entries
  }

  async findOneEntry(userId: string, entryId: string): Promise<EntryEntity> {
    const entry = await this.entryRepository.findOne({
      where: { id: entryId },
      relations: ['box', 'box.owner', 'expressions', 'expressions.strates']
    })

    return !entry || entry.box.owner.id !== userId ? null : entry
  }

  async createEntry(
    input: CreateEntryDTO,
    boxId: string,
    userId: string
  ): Promise<EntryEntity> {
    const owner = await this.get('user', userId)
    const box = await this.boxRepository.findOne({
      where: { id: boxId, owner }
    })

    if (!owner || !box) {
      throw new NotFoundException(`cannot find box n°${boxId}`)
    }
    const entry = await this.entryRepository.create({ ...input, box })
    await this.entryRepository.save(entry)
    return entry
  }

  async updateEntry(
    input: UpdateEntryDTO,
    entryId: string,
    userId: string
  ): Promise<EntryEntity> {
    const entry = await this.entryRepository.findOne({
      where: { id: entryId },
      relations: ['box', 'box.owner']
    })

    if (!entry || entry.box.owner.id !== userId)
      throw new NotFoundException(`cannot find entry n°${entryId}`)

    const updatedEntry = { ...entry, ...input }
    await this.entryRepository.save(updatedEntry)
    return updatedEntry
  }

  async deleteEntry(entryId: string, userId: string): Promise<EntryEntity> {
    const entry = await this.entryRepository.findOne({
      where: { id: entryId },
      relations: ['box', 'box.owner']
    })
    if (!entry || entry.box.owner.id !== userId)
      throw new NotFoundException(`cannot find entry n°${entryId}`)

    await this.entryRepository.delete(entry.id)
    return entry
  }

  private async get(model: string, id: string, relations?: string[]) {
    try {
      const repo = this[`${model.toLowerCase()}Repository`]
      if (!repo) throw new Error(`Repo for ${model} does not exist`)
      return await repo.findOneOrFail({
        where: { id },
        relations
      })
    } catch (e) {
      throw new NotFoundException(model + ' not found')
    }
  }
}
