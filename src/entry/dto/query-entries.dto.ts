import { IsOptional, IsNumber } from 'class-validator'
import { PaginationInput } from '../../graphql.schema'

export class PaginationInputDTO extends PaginationInput {
  @IsNumber()
  @IsOptional()
  take: number

  @IsNumber()
  @IsOptional()
  from: number
}
