import { IsString, MinLength, IsOptional } from 'class-validator'
import { CreateEntryInput } from '../../graphql.schema'

export class CreateEntryDTO extends CreateEntryInput {
  @IsOptional()
  @IsString()
  @MinLength(2)
  comment: string

  @IsOptional()
  expressions?: any[]
}
