import { IsString, MinLength, IsOptional } from 'class-validator'
import { UpdateEntryInput } from '../../graphql.schema'

export class UpdateEntryDTO extends UpdateEntryInput {
  @IsOptional()
  @IsString()
  @MinLength(2)
  comment: string
}
