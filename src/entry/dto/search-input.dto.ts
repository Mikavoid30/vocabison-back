import { IsOptional, IsString } from 'class-validator'
import { SearchInput } from '../../graphql.schema'

export class SearchInputDTO extends SearchInput {
  @IsString()
  @IsOptional()
  term: string
}
