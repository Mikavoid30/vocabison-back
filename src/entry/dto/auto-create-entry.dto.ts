import { IsString, MinLength, MaxLength, IsOptional } from 'class-validator'
import { AutoCreateEntriesInput } from '../../graphql.schema'

export class AutoCreateEntriesDTO extends AutoCreateEntriesInput {
  @IsString({ each: true })
  readonly expressions: string[]

  @IsString()
  @MinLength(2)
  @MaxLength(2)
  readonly source_lang: string

  @MaxLength(2, { each: true })
  @MinLength(2, { each: true })
  @IsString({ each: true })
  readonly destination_langs: string[]

  @IsOptional()
  readonly auto_translate: boolean

  @IsOptional()
  readonly include_defs: boolean
}
