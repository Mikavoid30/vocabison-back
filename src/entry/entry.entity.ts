import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne,
  OneToMany
} from 'typeorm'
import { BoxEntity } from '../box/box.entity'
import { ExpressionEntity } from '../expression/expression.entity'

@Entity('entry')
export class EntryEntity {
  @PrimaryGeneratedColumn() id: string
  @CreateDateColumn() created: Date
  @UpdateDateColumn() updated: Date

  @Column('text', { nullable: true }) comment: string
  @ManyToOne((type) => BoxEntity, (box) => box.entries, { onDelete: 'CASCADE' })
  box: BoxEntity

  @OneToMany((type) => ExpressionEntity, (expression) => expression.entry)
  expressions: ExpressionEntity[]
}
