import { Module } from '@nestjs/common'
import { EntryService } from './entry.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { BoxEntity } from '../box/box.entity'
import { BoxService } from '../box/box.service'
import { EntryEntity } from './entry.entity'
import { EntryResolver } from './entry.resolver'
import { DateScalar } from '../common/graphql/scalars/date.scalar'
import { UserEntity } from '../user/user.entity'
import { UserService } from '../user/user.service'
import { ExpressionEntity } from '../expression/expression.entity'
import { BisonService } from '../bison/bison.service'
import { CoreModule } from '../core/core.module'
import { ExpressionModule } from '../expression/expression.module'
import { ExpressionService } from '../expression/expression.service'
import { StrateModule } from '../strate/strate.module'
import { StrateEntity } from '../strate/strate.entity'
import { TranslatorModule } from '../translator/translator.module'

@Module({
  imports: [
    CoreModule,
    ExpressionModule,
    StrateModule,
    TranslatorModule,
    TypeOrmModule.forFeature([
      StrateEntity,
      EntryEntity,
      UserEntity,
      BoxEntity,
      ExpressionEntity
    ])
  ],
  controllers: [],
  providers: [
    EntryResolver,
    EntryService,
    DateScalar,
    BoxService,
    UserService,
    BisonService,
    ExpressionService
  ],
  exports: [EntryService, ExpressionService]
})
export class EntryModule {}
