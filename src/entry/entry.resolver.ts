import { Resolver, Query, Context, Args, Mutation } from '@nestjs/graphql'
import { SetMetadata, UseGuards } from '@nestjs/common'
import { Authorized } from '../common/guards/authorized.guard'
import { roles } from '../common/enums/roles.enum'
import { CreateEntryDTO } from './dto/create-entry.dto'
import { UpdateEntryDTO } from './dto/update-entry.dto'
import { EntryService } from './entry.service'
import { EntryEntity } from './entry.entity'
import { BisonService } from '../bison/bison.service'
import { PaginationInputDTO } from './dto/query-entries.dto'
import { SearchInputDTO } from './dto/search-input.dto'
import { AutoCreateEntriesDTO } from './dto/auto-create-entry.dto'

@Resolver('entry')
export class EntryResolver {
  constructor(
    private entryService: EntryService,
    private bisonService: BisonService
  ) {}

  @Query('entries')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  entries(
    @Args('boxId') boxId: string,
    @Args('paginationInput') paginationInput: PaginationInputDTO,
    @Args('searchInput') searchInput: SearchInputDTO,
    @Context() ctx: any
  ): Promise<EntryEntity[]> {
    return this.entryService.findAllFromBox(
      ctx.req.session.userId,
      boxId,
      paginationInput,
      searchInput
    )
  }

  @Query('entry')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  entry(
    @Args('entryId') id: string,
    @Context() ctx: any
  ): Promise<EntryEntity> {
    return this.entryService.findOneEntry(ctx.req.session.userId, id)
  }

  @Mutation('createEntry')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  async createEntry(
    @Context() ctx,
    @Args('boxId') boxId: string,
    @Args('createEntryInput') input: CreateEntryDTO
  ) {
    return this.entryService.createEntry(input, boxId, ctx.req.session.userId)
  }

  /** Will get definitions etc.. from an expression and a source & dest languages */
  @Mutation('autoCreateEntries')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  async autoCreateEntries(
    @Args('boxId') boxId: string,
    @Args('autoCreateEntriesInput') input: AutoCreateEntriesDTO,
    @Context() ctx: any
  ): Promise<EntryEntity[]> {
    // bison service
    return this.bisonService.autoCreateEntries(
      input,
      boxId,
      ctx.req.session.userId
    )
  }

  @Mutation('updateEntry')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  async updateEntry(
    @Context() ctx,
    @Args('entryId') entryId: string,
    @Args('updateEntryInput') input: UpdateEntryDTO
  ) {
    return this.entryService.updateEntry(input, entryId, ctx.req.session.userId)
  }

  @Mutation('deleteEntry')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  async deleteEntry(@Context() ctx, @Args('entryId') entryId: string) {
    return this.entryService.deleteEntry(entryId, ctx.req.session.userId)
  }
}
