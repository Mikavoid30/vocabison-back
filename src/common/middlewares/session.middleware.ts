import { Injectable, NestMiddleware } from '@nestjs/common'
import session from 'express-session'
import connectRedis from 'connect-redis'
import { ConfigService } from '../../config/config.service'
import { redis } from '../redis/redis'

const RedisStore = connectRedis(session)

@Injectable()
export class SessionMiddleware implements NestMiddleware {
  constructor(private configService: ConfigService) {}

  use = session({
    store: new RedisStore({
      client: redis as any
    }),
    name: 'qid',
    secret: this.configService.get('SESSION_SECRET'),
    resave: false,
    saveUninitialized: false,
    cookie: {
      httpOnly: true,
      secure: this.configService.get('NODE_ENV') === 'production',
      maxAge: 1000 * 60 * 60 * 24 * 7 * 365 // 7 years
    }
  })
}
