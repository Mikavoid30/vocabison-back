import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Logger
} from '@nestjs/common'
import { Observable } from 'rxjs'
import { tap } from 'rxjs/operators'
import { GqlExecutionContext } from '@nestjs/graphql'

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, call: CallHandler): Observable<any> {
    const req = context.switchToHttp().getRequest()
    const now = Date.now()
    if (req) {
      const method = req.method
      const url = req.url

      return call
        .handle()
        .pipe(
          tap(() =>
            Logger.log(
              `[${method}][${url}]${Date.now() - now}ms`,
              `${context.getClass().name} -> ${context.getHandler().name}`
            )
          )
        )
    } else {
      // GraphQL
      const ctx: any = GqlExecutionContext.create(context)
      const resolverName = ctx.constructorRef.name
      const infos = ctx.getInfo()
      return call
        .handle()
        .pipe(
          tap(() =>
            Logger.log(
              `[${infos.parentType}][${infos.fieldName}]${Date.now() - now}ms`,
              `${resolverName}`
            )
          )
        )
    }
  }
}
