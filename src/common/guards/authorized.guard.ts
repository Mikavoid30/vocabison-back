import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common'
import { GqlExecutionContext } from '@nestjs/graphql'
import { UserService } from '../../user/user.service'
import { Reflector } from '@nestjs/core'
import { roles } from '../enums/roles.enum'

@Injectable()
export class Authorized implements CanActivate {
  constructor(
    private readonly userService: UserService,
    private readonly reflector: Reflector
  ) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    let validRoles: roles[] = this.reflector.get<roles[]>(
      'roles',
      context.getHandler()
    )
    if (!validRoles || !validRoles.length) validRoles = [roles.USER]

    const ctx = GqlExecutionContext.create(context)
    const req = ctx.getContext().req
    if (!req.session.userId) return false
    const me = await this.userService.findOneById(req.session.userId)
    if (!me.confirmed || me.banned) return false
    return this.hasRole(me.roles, validRoles)
  }

  hasRole(userRoles: roles[], validRoles: roles[]) {
    return userRoles.some((role: roles) => validRoles.includes(role))
  }
}
