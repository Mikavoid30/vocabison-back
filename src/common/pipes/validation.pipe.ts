import {
  Injectable,
  PipeTransform,
  ArgumentMetadata,
  BadRequestException
} from '@nestjs/common'
import { validate } from 'class-validator'
import { plainToClass } from 'class-transformer'

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!value) return value // Session - GraphQL
    if (value instanceof Object && this.isEmpty(value)) {
      throw new BadRequestException('Validation failed')
    }

    if (!metatype || !this.toValidate(metatype)) return value

    const instance = plainToClass(metatype, value)
    const errors = await validate(instance)

    if (errors.length > 0) {
      throw new BadRequestException(
        errors.map((x) => ({ ...x.constraints, property: x.property }))
      )
    }
    return value
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object]
    return !types.includes(metatype)
  }

  private isEmpty(values: Object) {
    return Object.keys(values).length <= 0
  }
}
