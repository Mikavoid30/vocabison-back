import { mkdirp, remove } from 'fs-extra'
import path from 'path'

export function ensureDirectoryExistence(filePath: string = '') {
  return new Promise((resolve, reject) => {
    if (!filePath) return reject(new Error('no path provided'))
    mkdirp(filePath, (err: Error) => {
      if (err) return reject(new Error(err.message))
      resolve()
    })
  })
}

export function forceRemove(dir: string = 'data/_tmp') {
  if (!dir.endsWith('_tmp')) return
  remove(path.join('.', dir), (err: Error) => {
    console.log(err)
  })
}
