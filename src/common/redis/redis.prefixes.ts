const env = process.env.NODE_ENV || 'dev'
const pre = env.toUpperCase() + '-'

export const confirmationPrefix = pre + 'user-confirmation'
export const forgotPasswordPrefix = pre + 'forgot-password'
