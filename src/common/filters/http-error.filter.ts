import {
  Catch,
  ExceptionFilter,
  ArgumentsHost,
  HttpStatus,
  Logger,
  HttpException
} from '@nestjs/common'
import { Request } from 'express'
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError'
import { QueryFailedError } from 'typeorm'

@Catch(EntityNotFoundError, QueryFailedError, Error)
export class HttpErrorFilter implements ExceptionFilter {
  catch(
    exception: EntityNotFoundError | QueryFailedError | Error,
    host: ArgumentsHost
  ) {
    const ctx = host.switchToHttp()
    if (!ctx.getRequest<Request>()) {
      // GraphQL
      const statusCode =
        exception instanceof HttpException
          ? exception.getStatus()
          : HttpErrorFilter.getCustomStatus(exception)

      let { message }: { message: string | any } = exception || {
        message: null
      }
      let errorMessage = message.message
      if (Array.isArray(errorMessage)) {
        errorMessage = JSON.stringify(errorMessage[0].error)
      }

      // Log the error in console
      Logger.error(
        `[${message.error}][${statusCode}]`,
        errorMessage && errorMessage.length ? errorMessage : null,
        'HttpErrorFilter'
      )
    }
  }

  private static getCustomStatus(exception: Error) {
    let status = HttpStatus.INTERNAL_SERVER_ERROR
    if (exception instanceof EntityNotFoundError)
      status = HttpStatus.UNPROCESSABLE_ENTITY
    if (exception instanceof QueryFailedError) status = HttpStatus.NOT_FOUND
    return status
  }
}
