import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne
} from 'typeorm'
import { ExpressionEntity } from '../expression/expression.entity'

@Entity('strate')
export class StrateEntity {
  @PrimaryGeneratedColumn() id: string
  @CreateDateColumn() created: Date
  @UpdateDateColumn() updated: Date

  @Column('simple-array', { default: null, nullable: true }) items: string
  @Column('text') template: string
  @Column('text') type: string

  @ManyToOne((type) => ExpressionEntity, (expression) => expression.strates, {
    onDelete: 'CASCADE'
  })
  expression: ExpressionEntity
}

export const STRATE_TYPES = {
  COMMENTS: 'COMMENT',
  EXAMPLES: 'EXAMPLES',
  DEFINITIONS: 'DEFINITIONS',
  SYNONYMS: 'SYNONYMS'
}
