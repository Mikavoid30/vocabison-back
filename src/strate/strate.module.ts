import { Module } from '@nestjs/common'
import { StrateService } from './strate.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ExpressionEntity } from '../expression/expression.entity'
import { EntryEntity } from '../entry/entry.entity'
import { UserEntity } from '../user/user.entity'
import { StrateEntity } from './strate.entity'
import { DateScalar } from '../common/graphql/scalars/date.scalar'
import { UserService } from '../user/user.service'
import { StrateResolver } from './strate.resolver'

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ExpressionEntity,
      EntryEntity,
      StrateEntity,
      UserEntity
    ])
  ],
  providers: [StrateService, DateScalar, UserService, StrateResolver],
  exports: [StrateService]
})
export class StrateModule {}
