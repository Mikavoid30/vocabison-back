import { IsString, MinLength } from 'class-validator'
import { CreateStrateInput } from '../../graphql.schema'

export class CreateStrateDTO extends CreateStrateInput {
  @IsString({ each: true })
  readonly items: string[]

  @IsString()
  @MinLength(2)
  readonly type: string

  @IsString()
  @MinLength(2)
  readonly template: string
}
