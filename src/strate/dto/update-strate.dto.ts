import { IsString, MinLength, IsOptional } from 'class-validator'
import { UpdateStrateInput } from '../../graphql.schema'

export class UpdateStrateDTO extends UpdateStrateInput {
  @IsOptional()
  @IsString({ each: true })
  readonly items: string[]

  @IsOptional()
  @IsString()
  @MinLength(2)
  readonly type: string

  @IsOptional()
  @IsString()
  @MinLength(2)
  readonly template: string
}
