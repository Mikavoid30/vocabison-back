import { Resolver, Query, Args, Context, Mutation } from '@nestjs/graphql'
import { StrateService } from './strate.service'
import { roles } from '../common/enums/roles.enum'
import { SetMetadata, UseGuards, Body } from '@nestjs/common'
import { Authorized } from '../common/guards/authorized.guard'
import { Strate } from '../graphql.schema'
import { CreateStrateDTO } from './dto/create-strate.dto'
import { UpdateStrateDTO } from './dto/update-strate.dto'

@Resolver('strate')
export class StrateResolver {
  constructor(private strateService: StrateService) {}

  @Query('strates')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  strates(
    @Args('expressionId') expressionId: string,
    @Args('entryId') entryId: string,
    @Context() ctx: any
  ): Promise<Strate[]> {
    return this.strateService.findAll(
      entryId,
      expressionId,
      ctx.req.session.userId
    )
  }

  @Query('strate')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  strate(
    @Args('entryId') entryId: string,
    @Args('strateId') strateId: string,
    @Context() ctx: any
  ): Promise<Strate[]> {
    return this.strateService.findOne(entryId, strateId, ctx.req.session.userId)
  }

  @Mutation('createStrate')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  createStrate(
    @Args('entryId') entryId: string,
    @Args('expressionId') expressionId: string,
    @Args('createStrateInput') inputs: CreateStrateDTO,
    @Context() ctx: any
  ) {
    return this.strateService.createStrate(
      entryId,
      expressionId,
      ctx.req.session.userId,
      inputs
    )
  }

  @Mutation('updateStrate')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  updateStrate(
    @Args('entryId') entryId: string,
    @Args('strateId') strateId: string,
    @Args('updateStrateInput') inputs: UpdateStrateDTO,
    @Context() ctx: any
  ) {
    return this.strateService.updateStrate(
      entryId,
      strateId,
      ctx.req.session.userId,
      inputs
    )
  }

  @Mutation('deleteStrate')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  deleteStrate(
    @Args('entryId') entryId: string,
    @Args('strateId') strateId: string,
    @Context() ctx: any
  ) {
    return this.strateService.deleteStrate(
      entryId,
      strateId,
      ctx.req.session.userId
    )
  }
}
