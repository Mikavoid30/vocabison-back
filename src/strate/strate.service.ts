import { Injectable, NotFoundException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { EntryEntity } from '../entry/entry.entity'
import { Repository } from 'typeorm'
import { StrateEntity } from './strate.entity'
import { CreateStrateDTO } from './dto/create-strate.dto'
import { UpdateStrateDTO } from './dto/update-strate.dto'
import { ExpressionEntity } from '../expression/expression.entity'

@Injectable()
export class StrateService {
  constructor(
    @InjectRepository(StrateEntity)
    private strateRepository: Repository<StrateEntity>,
    @InjectRepository(EntryEntity)
    private entryRepository: Repository<EntryEntity>,
    @InjectRepository(ExpressionEntity)
    private expressionRepository: Repository<ExpressionEntity>
  ) {}

  async findAll(entryId: string, expressionId: string, userId: string) {
    const entry = await this.entryRepository.findOne({
      where: { id: entryId },
      relations: ['expressions', 'box', 'box.owner']
    })

    if (!entry || entry.box.owner.id !== userId) {
      throw new NotFoundException(`cannot find entry n°${entryId}`)
    }

    const expression = await this.get('expression', expressionId, ['strates'])
    return expression.strates
  }

  async findOne(entryId: string, strateId: string, userId: string) {
    const entry = await this.entryRepository.findOne({
      where: { id: entryId },
      relations: ['expressions', 'box', 'box.owner']
    })

    if (!entry || entry.box.owner.id !== userId) {
      throw new NotFoundException(`cannot find entry n°${entryId}`)
    }

    const strate = await this.get('strate', strateId)
    return strate
  }

  async createStrate(
    entryId: string,
    expressionId: string,
    userId: string,
    inputs: CreateStrateDTO
  ) {
    try {
      const entry = await this.entryRepository.findOne({
        where: { id: entryId },
        relations: ['expressions', 'box', 'box.owner']
      })

      if (!entry || entry.box.owner.id !== userId) {
        throw new NotFoundException(`cannot find entry n°${entryId}`)
      }

      const expression = await this.get('expression', expressionId, ['strates'])
      if (
        expression.strates &&
        expression.strates.some((x: any) => x.type === inputs.type)
      ) {
        throw new NotFoundException(
          `cannot create strate of type ${inputs.type}, it already exists`
        )
      }

      const strate = await this.strateRepository.create({
        ...inputs,
        expression
      } as any)
      await this.strateRepository.save(strate)
      return strate
    } catch (e) {
      console.log('error', e.message)
    }
  }

  async updateStrate(
    entryId: string,
    strateId: string,
    userId: string,
    inputs: Partial<UpdateStrateDTO>
  ) {
    const entry = await this.entryRepository.findOne({
      where: { id: entryId },
      relations: ['expressions', 'box', 'box.owner']
    })

    if (!entry || entry.box.owner.id !== userId) {
      throw new NotFoundException(`cannot find entry n°${entryId}`)
    }

    const strate = await this.get('strate', strateId)
    const updatedStrate = { ...strate, ...inputs }
    await this.strateRepository.save(updatedStrate)
    return updatedStrate
  }

  async deleteStrate(entryId: string, strateId: string, userId: string) {
    const entry = await this.entryRepository.findOne({
      where: { id: entryId },
      relations: ['expressions', 'box', 'box.owner']
    })

    if (!entry || entry.box.owner.id !== userId) {
      throw new NotFoundException(`cannot find entry n°${entryId}`)
    }

    const strate = await this.get('strate', strateId)
    await this.strateRepository.delete(strate.id)
    return strate
  }

  private async get(model: string, id: string, relations?: string[]) {
    try {
      const repo = this[`${model.toLowerCase()}Repository`]
      if (!repo) throw new Error(`Repo for ${model} does not exist`)
      return await repo.findOneOrFail({
        where: { id },
        relations
      })
    } catch (e) {
      throw new NotFoundException(model + ' not found')
    }
  }
}
