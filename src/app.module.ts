import { Module, MiddlewareConsumer } from '@nestjs/common'
import { GraphQLModule } from '@nestjs/graphql'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { AppResolver } from './app.resolver'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserModule } from './user/user.module'
import { APP_PIPE, APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core'
import { ValidationPipe } from './common/pipes/validation.pipe'
import { LoggingInterceptor } from './common/interceptors/logging.interceptor'
import { HttpErrorFilter } from './common/filters/http-error.filter'
import { SessionMiddleware } from './common/middlewares/session.middleware'
import { join } from 'path'
import { ConfigModule } from './config/config.module'
import { BoxModule } from './box/box.module'
import { EntryModule } from './entry/entry.module'
import { ExpressionModule } from './expression/expression.module'
import { StrateModule } from './strate/strate.module'
import { BisonModule } from './bison/bison.module'
import { TranslatorModule } from './translator/translator.module'
import { PdfModule } from './pdf/pdf.module'
import { FicheModule } from './fiche/fiche.module'
import { FileManagerModule } from './file-manager/file-manager.module'

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      context: ({ req, res }) => ({ req, res }),
      definitions: {
        path: join(process.cwd(), 'src/graphql.schema.ts'),
        outputAs: 'class'
      },
      cors: {
        credentials: true,
        origin: 'http://localhost:3000'
      }
    }),
    UserModule,
    ConfigModule,
    EntryModule,
    BoxModule,
    ExpressionModule,
    StrateModule,
    BisonModule,
    TranslatorModule,
    PdfModule,
    FicheModule,
    FileManagerModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    AppResolver,
    {
      provide: APP_PIPE,
      useClass: ValidationPipe
    },
    {
      provide: APP_FILTER,
      useClass: HttpErrorFilter
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor
    }
  ]
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(SessionMiddleware).forRoutes('')
  }
}
