import { Module } from '@nestjs/common'
import { MailerService } from './mailer/mailer.service'
import { ConfigService } from '../config/config.service'
import { LanguagesService } from './languages/languages.service'

@Module({
  providers: [
    MailerService,
    LanguagesService,
    {
      provide: ConfigService,
      useValue: new ConfigService(`.${process.env.NODE_ENV || 'dev'}.env`)
    }
  ],
  exports: [MailerService, LanguagesService]
})
export class CoreModule {}
