import { Injectable } from '@nestjs/common'

@Injectable()
export class LanguagesService {
  private availableLanguages: any[]
  private validLanguageCodes: any[]

  constructor() {
    this.availableLanguages = [
      { key: 'Arabic', text: 'Arabic', value: 'ar' },
      { key: 'Chinese', text: 'Chinese', value: 'zh' },
      { key: 'Danish', text: 'Danish', value: 'da' },
      { key: 'Dutch', text: 'Dutch', value: 'nl' },
      { key: 'English', text: 'English', value: 'en' },
      { key: 'French', text: 'French', value: 'fr' },
      { key: 'German', text: 'German', value: 'de' },
      { key: 'Greek', text: 'Greek', value: 'el' },
      { key: 'Hungarian', text: 'Hungarian', value: 'hu' },
      { key: 'Italian', text: 'Italian', value: 'it' },
      { key: 'Japanese', text: 'Japanese', value: 'ja' },
      { key: 'Korean', text: 'Korean', value: 'ko' },
      { key: 'Lithuanian', text: 'Lithuanian', value: 'lt' },
      { key: 'Polish', text: 'Polish', value: 'pl' },
      { key: 'Portuguese', text: 'Portuguese', value: 'pt' },
      { key: 'Russian', text: 'Russian', value: 'ru' },
      { key: 'Spanish', text: 'Spanish', value: 'es' },
      { key: 'Swedish', text: 'Swedish', value: 'sv' },
      { key: 'Turkish', text: 'Turkish', value: 'tr' },
      { key: 'Vietnamese', text: 'Vietnamese', value: 'vi' }
    ]

    this.validLanguageCodes = this.availableLanguages.map((x) =>
      x.value.toUpperCase()
    )
  }

  getLangFromCode(code: string = 'en') {
    if (!this.isValidCode(code)) return code
    return this.availableLanguages.filter(
      (x) => x.value === code.toLowerCase()
    )[0].text
  }

  isValidCode(lang_code: string = '') {
    return this.validLanguageCodes.indexOf(lang_code.toUpperCase()) >= 0
  }
}
