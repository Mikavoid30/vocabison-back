import { Injectable, Logger } from '@nestjs/common'
import nodemailer from 'nodemailer'
import { ConfigService } from '../../config/config.service'

@Injectable()
export class MailerService {
  private testAccount: any
  private transporter: any
  private mailer = nodemailer
  private conf: { from: string }
  private isTest: boolean

  constructor(private readonly configService: ConfigService) {
    this.conf = {
      from: '"Vocabison Accounts" <contact@vocabison.com>'
    }
    this.isTest = this.configService.get('NODE_ENV') === 'test'
  }

  async init() {
    this.testAccount = await this.mailer.createTestAccount()
    this.transporter = this.mailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: this.testAccount.user, // generated ethereal user
        pass: this.testAccount.pass // generated ethereal password
      }
    })
  }

  async sendConfirmationEmail(
    to: string = this.configService.get('ADMIN_EMAIL'),
    url: string
  ) {
    if (this.isTest) return
    await this.init()
    const info = await this.transporter.sendMail({
      from: this.conf.from,
      to: [to], // list of receivers
      subject: '✔ Confirm your email please =)', // Subject line
      text: 'Hello world?', // plain text body
      html: `<b>Click the link below to confirm your account</b><br/><a href="${url}">Confirm my account</a>` // html body
    })
    this.log(info, 'Mailer(confirm account)')
  }

  async sendForgotPasswordEmail(
    to: string = this.configService.get('ADMIN_EMAIL'),
    url: string
  ) {
    if (this.isTest) return
    await this.init()
    const info = await this.transporter.sendMail({
      from: this.conf.from,
      to: [to], // list of receivers
      subject: '✔ Change your password =)', // Subject line
      text: url, // plain text body
      html: `<b>Click the link below to change your password</b><br/><a href="${url}">Change my password</a>` // html body
    })
    this.log(info, 'Mailer(forgot password)')
  }

  log(info: any, context: string) {
    Logger.log(`Message sent: ${info.messageId}`, context)
    Logger.log(`Preview URL: ${this.mailer.getTestMessageUrl(info)}`, context)
  }
}
