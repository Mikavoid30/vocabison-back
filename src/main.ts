import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { Logger } from '@nestjs/common'
import { config } from 'dotenv'

async function bootstrap() {
  config({ path: `.${process.env.NODE_ENV || 'dev'}.env` })
  const port = process.env.PORT
  const app = await NestFactory.create(AppModule)

  if (process.env.NODE_ENV !== 'production') {
    app.use((req: any, res: any, next: any) => {
      // Debug purpose
      next()
    })
  }

  await app.listen(port, () => {
    Logger.log(`Listening on port ${port}`, 'Bootstrap')
  })
}
bootstrap()
