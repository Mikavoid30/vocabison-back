
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export class AutoCreateEntriesInput {
    expressions?: string[];
    auto_translate?: boolean;
    include_defs?: boolean;
    source_lang: string;
    destination_langs: string[];
}

export class ConfirmUserInput {
    token: string;
}

export class CreateBoxInput {
    title: string;
    languages: string[];
    tags?: string[];
    comment?: string;
}

export class CreateEntryInput {
    comment?: string;
    expressions?: CreateExpressionInput[];
}

export class CreateExpressionInput {
    text: string;
    lang: string;
    definitions?: string[];
    synonyms?: string[];
    examples?: string[];
}

export class CreateFicheInput {
    boxId: string;
    title: string;
    visibleLanguages: string[];
    date?: string;
    showComment?: boolean;
    showType?: boolean;
    nbDefs?: number;
    nbExamples?: number;
    nbSynonyms?: number;
    nbTranslations?: number;
    nbMaxEntries?: number;
}

export class CreateStrateInput {
    items?: string[];
    type: string;
    template: string;
}

export class LoginUserInput {
    email: string;
    password: string;
}

export class PaginationInput {
    take?: number;
    from?: number;
}

export class RegisterUserInput {
    email: string;
    password: string;
    native_language: string;
}

export class SearchInput {
    term: string;
}

export class UpdateBoxInput {
    title?: string;
    languages?: string[];
    tags?: string[];
    comment?: string;
    slug?: string;
}

export class UpdateEntryInput {
    comment?: string;
}

export class UpdateExpressionInput {
    text?: string;
    lang?: string;
    definitions?: string[];
    synonyms?: string[];
    examples?: string[];
}

export class UpdateFicheInput {
    boxId?: string;
    title?: string;
}

export class UpdateStrateInput {
    items?: string[];
    type?: string;
    template?: string;
}

export class Bison {
    test?: string;
}

export class Box {
    id: string;
    created: Date;
    updated: Date;
    title: string;
    tags: string[];
    slug: string;
    comment?: string;
    languages: string[];
    owner?: User;
}

export class Entry {
    id: string;
    created: Date;
    updated: Date;
    comment?: string;
    box: Box;
    expressions?: Expression[];
}

export class Export {
    path?: string;
    url?: string;
    type?: FileType;
}

export class Expression {
    id: string;
    created: Date;
    updated: Date;
    text: string;
    lang: string;
    strates?: Strate[];
}

export class Fiche {
    id: string;
    created: Date;
    updated: Date;
    boxId: string;
    date?: string;
    visibleLanguages: string[];
    title: string;
    bisonFileToken?: string;
    showComment?: boolean;
    showType?: boolean;
    nbDefs?: number;
    nbExamples?: number;
    nbSynonyms?: number;
    nbTranslations?: number;
    nbMaxEntries?: number;
    owner: User;
    export?: Export;
}

export class FileType {
    name?: string;
    ext?: string;
}

export abstract class IMutation {
    abstract createBox(createBoxInput?: CreateBoxInput): Box | Promise<Box>;

    abstract updateBox(boxId?: string, updateBoxInput?: UpdateBoxInput): Box | Promise<Box>;

    abstract deleteBox(boxId?: string): Box | Promise<Box>;

    abstract autoCreateEntries(boxId: string, autoCreateEntriesInput?: AutoCreateEntriesInput): Entry[] | Promise<Entry[]>;

    abstract createEntry(boxId: string, createEntryInput?: CreateEntryInput): Entry | Promise<Entry>;

    abstract updateEntry(entryId?: string, updateEntryInput?: UpdateEntryInput): Entry | Promise<Entry>;

    abstract deleteEntry(entryId?: string): Entry | Promise<Entry>;

    abstract createExpression(boxId: string, entryId: string, auto?: boolean, createExpressionInput?: CreateExpressionInput): Expression | Promise<Expression>;

    abstract updateExpression(entryId: string, expressionId: string, updateExpressionInput?: UpdateExpressionInput): Expression | Promise<Expression>;

    abstract deleteExpression(entryId: string, expressionId?: string): Expression | Promise<Expression>;

    abstract createFiche(createFicheInput?: CreateFicheInput): Fiche | Promise<Fiche>;

    abstract updateFiche(ficheId: string, updateFicheInput?: UpdateFicheInput): Fiche | Promise<Fiche>;

    abstract createStrate(entryId: string, expressionId: string, createStrateInput?: CreateStrateInput): Strate | Promise<Strate>;

    abstract updateStrate(entryId: string, strateId: string, updateStrateInput?: UpdateStrateInput): Strate | Promise<Strate>;

    abstract deleteStrate(entryId: string, strateId: string): Strate | Promise<Strate>;

    abstract register(registerInput: RegisterUserInput): User | Promise<User>;

    abstract login(loginInput: LoginUserInput): User | Promise<User>;

    abstract logout(): boolean | Promise<boolean>;

    abstract confirm(token?: string): boolean | Promise<boolean>;

    abstract forgotPassword(email?: string): boolean | Promise<boolean>;

    abstract changePassword(token: string, password: string): boolean | Promise<boolean>;

    abstract ban(id: string): string | Promise<string>;

    abstract unban(id: string): string | Promise<string>;
}

export abstract class IQuery {
    abstract hello(): string | Promise<string>;

    abstract helloUser(): string | Promise<string>;

    abstract helloAdmin(): string | Promise<string>;

    abstract helloPro(): string | Promise<string>;

    abstract bison(): Bison | Promise<Bison>;

    abstract box(slug: string): Box | Promise<Box>;

    abstract boxById(id: string): Box | Promise<Box>;

    abstract boxes(): Box[] | Promise<Box[]>;

    abstract myBoxes(): Box[] | Promise<Box[]>;

    abstract entries(boxId: string, paginationInput?: PaginationInput, searchInput?: SearchInput): Entry[] | Promise<Entry[]>;

    abstract entry(entryId: string): Entry | Promise<Entry>;

    abstract expressions(entryId: string, n?: number, p?: number): Expression[] | Promise<Expression[]>;

    abstract expression(entryId: string, expressionId: string): Expression | Promise<Expression>;

    abstract fiche(ficheId: string): Fiche | Promise<Fiche>;

    abstract fiches(): Fiche[] | Promise<Fiche[]>;

    abstract strates(entryId: string, expressionId: string): Strate[] | Promise<Strate[]>;

    abstract strate(entryId: string, strateId: string): Strate | Promise<Strate>;

    abstract users(): User[] | Promise<User[]>;

    abstract me(): User | Promise<User>;
}

export class Strate {
    id: string;
    created: Date;
    updated: Date;
    items?: string[];
    type: string;
    template: string;
}

export class User {
    id: string;
    created: Date;
    email: string;
    native_language: string;
    fiches?: Fiche[];
    boxes?: Box[];
}
