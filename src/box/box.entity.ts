import slugify from 'slugify'
import shortid from 'shortid'

import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  BeforeInsert,
  ManyToOne,
  OneToMany,
  BeforeUpdate
} from 'typeorm'
import { InternalServerErrorException } from '@nestjs/common'
import { UserEntity } from '../user/user.entity'
import { EntryEntity } from '../entry/entry.entity'

@Entity('box')
export class BoxEntity {
  @PrimaryGeneratedColumn() id: string
  @CreateDateColumn() created: Date
  @UpdateDateColumn() updated: Date

  @Column('text') title: string
  @Column('simple-array', { default: [] }) tags: string[]
  @Column('text', { unique: true }) slug: string
  @Column('text', { nullable: true }) comment: string
  @Column('simple-array', { default: [] }) languages: string[]
  @ManyToOne((type) => UserEntity, (user) => user.boxes)
  owner: UserEntity

  @OneToMany((type) => EntryEntity, (entry) => entry.box)
  entries: EntryEntity[]
  boxes: BoxEntity[]

  @BeforeInsert()
  async slugify() {
    try {
      this.slug = slugify(this.title + '-' + shortid.generate(), {
        lower: true
      })
      if (!this.languages) {
        this.languages = [this.owner.native_language]
      }
      // Unify languages
      const set = new Set(this.languages.map((x) => x.toLowerCase()))
      this.languages = [...set]
    } catch (e) {
      throw new InternalServerErrorException('Impossible to save the list')
    }
  }
}
