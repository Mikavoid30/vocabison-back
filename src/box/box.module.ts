import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserEntity } from '../user/user.entity'
import { BoxEntity } from './box.entity'
import { BoxResolver } from './box.resolver'
import { DateScalar } from '../common/graphql/scalars/date.scalar'
import { BoxService } from './box.service'
import { UserService } from '../user/user.service'
import { EntryService } from '../entry/entry.service'
import { EntryEntity } from '../entry/entry.entity'

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, BoxEntity, EntryEntity])],
  controllers: [],
  providers: [BoxResolver, BoxService, DateScalar, UserService, EntryService],
  exports: [BoxService]
})
export class BoxModule {}
