import { Injectable, NotFoundException } from '@nestjs/common'
import { Repository } from 'typeorm'
import { BoxEntity } from './box.entity'
import { InjectRepository } from '@nestjs/typeorm'
import { CreateBoxInput, UpdateBoxInput, Box } from '../graphql.schema'
import { UserEntity } from '../user/user.entity'
import { EntryEntity } from '../entry/entry.entity'

@Injectable()
export class BoxService {
  constructor(
    @InjectRepository(BoxEntity) private boxRepository: Repository<BoxEntity>,
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    @InjectRepository(EntryEntity)
    private entryRepository: Repository<EntryEntity>
  ) {}

  async showAll(): Promise<Box[]> {
    return await this.boxRepository.find({ relations: ['owner'] })
  }

  async findBySlug(slug: string, userId: string): Promise<Box> {
    const owner = await this.get('user', userId)
    const box = await this.boxRepository.findOne({
      where: { slug, owner },
      relations: ['owner']
    })
    if (!box) {
      throw new NotFoundException(`cannot find box ${slug}`)
    }
    return box
  }

  async findById(
    id: string,
    userId: string,
    includeEntries: boolean = false
  ): Promise<Box> {
    const inclusions = includeEntries ? ['entries', 'entries.expressions'] : []
    const owner = await this.get('user', userId)
    const box = await this.boxRepository.findOne({
      where: { id, owner },
      relations: ['owner', ...inclusions]
    })
    if (!box) {
      throw new NotFoundException(`cannot find box ${id}`)
    }
    return box
  }

  async showAllFromUser(userId: string): Promise<Box[]> {
    const user = await this.get('user', userId)
    return await this.boxRepository.find({
      where: { owner: user },
      order: { updated: 'DESC' },
      relations: ['owner']
    })
  }

  async createBox(input: CreateBoxInput, userId: string): Promise<Box> {
    const owner = await this.get('user', userId)
    const box = await this.boxRepository.create({ ...input, owner })
    box.languages = this.unifyLanguages(box.languages)
    await this.boxRepository.save(box)
    return box
  }

  async updateBox(
    input: UpdateBoxInput,
    boxId: string,
    userId: string
  ): Promise<Box> {
    const owner = await this.get('user', userId)
    const box = await this.boxRepository.findOne({ id: boxId, owner })
    if (!owner || !box) {
      throw new NotFoundException(`cannot find box n°${boxId}`)
    }
    const updatedBox = { ...box, ...input }
    updatedBox.languages = this.unifyLanguages(updatedBox.languages)
    await this.boxRepository.save(updatedBox)
    return updatedBox
  }

  async deleteBox(boxId: string, userId: string): Promise<Box> {
    const owner = await this.get('user', userId)
    const box = await this.boxRepository.findOne({ id: boxId, owner })
    if (!owner || !box) {
      throw new NotFoundException(`cannot find box n°${boxId}`)
    }
    await this.boxRepository.delete(box.id)
    return box
  }

  private unifyLanguages(languages: string[] = []) {
    try {
      if (!languages || !languages.length) return []
      // Unify languages
      const set = new Set(languages.map((x) => x.toLowerCase()))
      return [...set]
    } catch (e) {
      return []
    }
  }

  private async get(model: string, id: string, relations?: string[]) {
    try {
      const repo = this[`${model.toLowerCase()}Repository`]
      if (!repo) throw new Error(`Repo for ${model} does not exist`)
      return await repo.findOneOrFail({
        where: { id },
        relations
      })
    } catch (e) {
      throw new NotFoundException(model + ' not found')
    }
  }
}
