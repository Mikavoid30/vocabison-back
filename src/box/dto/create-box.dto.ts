import { IsString, MinLength, MaxLength, IsOptional } from 'class-validator'
import { CreateBoxInput } from '../../graphql.schema'

export class CreateBoxDTO extends CreateBoxInput {
  @IsString()
  @MinLength(1)
  readonly title: string

  @MaxLength(2, { each: true })
  @MinLength(2, { each: true })
  readonly languages: string[]

  @IsOptional()
  @MaxLength(50, { each: true })
  @MinLength(1, { each: true })
  readonly tags: string[]

  @IsOptional()
  @IsString()
  @MinLength(1)
  comment: string
}
