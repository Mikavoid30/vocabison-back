import { IsString, MinLength, MaxLength, IsOptional } from 'class-validator'
import { UpdateBoxInput } from '../../graphql.schema'

export class UpdateBoxDTO extends UpdateBoxInput {
  @IsString()
  @IsOptional()
  @MinLength(1)
  readonly title: string

  @IsOptional()
  @MaxLength(2, { each: true })
  @MinLength(2, { each: true })
  readonly languages: string[]

  @IsOptional()
  @MaxLength(50, { each: true })
  @MinLength(1, { each: true })
  readonly tags: string[]

  @IsOptional()
  @IsString()
  @MinLength(1)
  comment: string

  @IsOptional()
  @IsString()
  @MinLength(2)
  @MaxLength(256)
  slug: string
}
