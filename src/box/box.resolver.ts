import { Resolver, Query, Args, Context, Mutation } from '@nestjs/graphql'
import { BoxService } from './box.service'
import { SetMetadata, UseGuards } from '@nestjs/common'
import { Authorized } from '../common/guards/authorized.guard'
import { roles } from '../common/enums/roles.enum'
import { CreateBoxDTO } from './dto/create-box.dto'
import { UpdateBoxDTO } from './dto/update-box.dto'

@Resolver('box')
export class BoxResolver {
  constructor(private boxService: BoxService) {}

  @Query('boxes')
  @SetMetadata('roles', [roles.ADMIN])
  @UseGuards(Authorized)
  boxes() {
    return this.boxService.showAll()
  }

  @Query('box')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  box(@Args('slug') slug: string, @Context() ctx: any) {
    return this.boxService.findBySlug(slug, ctx.req.session.userId)
  }

  @Query('boxById')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  boxbyId(@Args('id') id: string, @Context() ctx: any) {
    return this.boxService.findById(id, ctx.req.session.userId)
  }

  @Query('myBoxes')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  myBoxes(@Context() ctx) {
    return this.boxService.showAllFromUser(ctx.req.session.userId)
  }

  @Mutation('createBox')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  async createBox(@Context() ctx, @Args('createBoxInput') input: CreateBoxDTO) {
    return this.boxService.createBox(input, ctx.req.session.userId)
  }

  @Mutation('updateBox')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  async updateBox(
    @Context() ctx,
    @Args('boxId') boxId: string,
    @Args('updateBoxInput') input: UpdateBoxDTO
  ) {
    return this.boxService.updateBox(input, boxId, ctx.req.session.userId)
  }

  @Mutation('deleteBox')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  async deleteBox(@Context() ctx, @Args('boxId') boxId: string) {
    return this.boxService.deleteBox(boxId, ctx.req.session.userId)
  }
}
