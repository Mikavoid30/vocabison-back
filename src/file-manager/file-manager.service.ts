import { Injectable } from '@nestjs/common'
import shortid from 'shortid'
import fs from 'fs'
import path from 'path'
import targz from 'targz'
import {
  ensureDirectoryExistence,
  forceRemove
} from 'src/common/utils/filesystem'

const BISON_DIR = path.join('data', 'bisonFiles')
const TMP_DIR = path.join('data', '_TMP_')

type SimplifiedEntries = {
  exps: {
    w: string // word
    t: string // type
    o: string[] // other translatios
    l: string // lang
    c: string // comment
    s: string[] // synonyms
    e: string[] // examples
    d: string[] // defs
  }
}

type BisonFileData = {
  meta: {
    title: string
    date: string
    author: string
    isPro: boolean
    tagline: string
    langs: string[]
    nbLangs: number
  }
  entries: SimplifiedEntries[]
}

@Injectable()
export class FileManagerService {
  getBisonData(token: string = '') {
    return new Promise((resolve, reject) => {
      if (!token) return reject(new Error('No file token provided'))
      if (!fs.existsSync(path.join(BISON_DIR, token)))
        return reject(new Error('Token is invalid'))
      const dir = path.join(BISON_DIR, token)

      const tmpPath = path.join(dir, '_tmp')
      targz.decompress(
        {
          src: path.join(dir, 'export.json.tar.gz'),
          dest: tmpPath
        },
        (err: Error) => {
          if (err) reject(err)
          const filePath = path.join(tmpPath, 'vocabison-export.json')

          fs.readFile(filePath, (err: Error, data) => {
            if (err) reject(err)

            const jsObject = JSON.parse(String(data))
            forceRemove(tmpPath)
            resolve(jsObject)
          })
        }
      )
    })
  }

  /**
   * Will create a file and then remove it after deleteTimeout secondes
   * @param filename
   * @param readable
   * @param deleteTimeout
   */
  async saveAutoDeleteFile(
    filename: string,
    readable = null,
    deleteTimeout = 30
  ) {
    if (!filename) return Promise.resolve()
    try {
      await ensureDirectoryExistence(TMP_DIR)
      const filePath = path.join(TMP_DIR, filename)
      readable = readable || fs.createReadStream(filePath)
      readable.pipe(fs.createWriteStream(filePath))
      readable.end()
      setTimeout(() => {
        try {
          fs.unlinkSync(filePath)
        } catch (e) {
          console.log('File was already deleted')
        }
      }, deleteTimeout * 1000)
      return Promise.resolve(filePath)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  saveBisonFile(
    bisonData: BisonFileData,
    token: string = '',
    dir: string = BISON_DIR
  ) {
    return new Promise(async (resolve, reject) => {
      const bstoken = token || shortid()
      dir = path.join(dir, bstoken)
      const tmpPath = path.join(dir, '_tmp')
      await ensureDirectoryExistence(tmpPath)
      const filePath = path.join(dir, '_tmp', `vocabison-export.json`)
      const compressedfilePath = path.join(dir, `export.json.tar.gz`)
      fs.writeFile(
        filePath,
        JSON.stringify(
          bisonData,
          null,
          process.env.NODE_ENV === 'production' ? 0 : 2
        ),
        async (err) => {
          if (err) reject(new Error(err.message))
          targz.compress(
            {
              src: tmpPath,
              dest: compressedfilePath
            },
            (err: Error) => {
              if (err) return reject(err.message)
              forceRemove(tmpPath)
              resolve(bstoken)
            }
          )
        }
      )
    })
  }

  removeFile(file: string) {
    if (!file) return
    return fs.unlinkSync(file)
  }
}
