import { Resolver, Query } from '@nestjs/graphql'
import { UseGuards, SetMetadata } from '@nestjs/common'
import { Authorized } from './common/guards/authorized.guard'
import { roles } from './common/enums/roles.enum'

@Resolver('app')
export class AppResolver {
  @Query()
  hello() {
    return 'Hello World - Public'
  }

  @Query()
  @UseGuards(Authorized)
  helloUser() {
    return 'Hello World - User role'
  }

  @Query()
  @SetMetadata('roles', [roles.PRO])
  @UseGuards(Authorized)
  helloPro() {
    return 'Hello World - Pro role'
  }

  @Query()
  @SetMetadata('roles', [roles.ADMIN])
  @UseGuards(Authorized)
  helloAdmin() {
    return 'Hello World - Admin role'
  }
}
