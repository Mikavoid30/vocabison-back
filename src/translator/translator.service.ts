import { Injectable } from '@nestjs/common'
import { Translate } from '@google-cloud/translate'
import { TranslatorResponse } from './translator.interface'
@Injectable()
export class TranslatorService {
  google: any

  fakeTranslate(expression: string, to: string) {
    return Promise.resolve(`${expression}-${to}`)
  }

  async getTranslations(
    sourceLang: string,
    expression: string,
    destinationLangs: string[]
  ): Promise<TranslatorResponse[]> {
    const translations = [{ text: expression, lang: sourceLang }]
    const length = destinationLangs.length
    try {
      for (let i = 0; i < length; i++) {
        if (process.env.DO_FAKE_TRANSLATIONS === 'true') {
          const text = await this.fakeTranslate(expression, destinationLangs[i])
          translations.push({ text, lang: destinationLangs[i] })
        } else {
          this.google = new Translate()
          const [text] = await this.google.translate(expression, {
            from: sourceLang,
            to: destinationLangs[i]
          })
          translations.push({ text, lang: destinationLangs[i] })
        }
      }
      return translations
    } catch (e) {
      console.error(e)
    }
  }
}
