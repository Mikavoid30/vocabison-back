import { Module } from '@nestjs/common'
import { CoreModule } from '../core/core.module'
import { TranslatorService } from './translator.service'

@Module({
  imports: [CoreModule],
  providers: [TranslatorService],
  exports: [TranslatorService]
})
export class TranslatorModule {}
