export type TranslatorResponse = {
  lang: string
  text: string
}
