import { EntryEntity } from 'src/entry/entry.entity'
import { ExpressionEntity } from '../expression/expression.entity'
import { AutoCreateEntriesDTO } from '../entry/dto/auto-create-entry.dto'
import { StrateEntity } from '../strate/strate.entity'
import { CreateStrateDTO } from '../strate/dto/create-strate.dto'

export interface IBison {
  autoCreateEntries: (
    input: AutoCreateEntriesDTO,
    boxId: string,
    userId: string
  ) => null | Promise<Partial<EntryEntity[]>>
  buildExpressions: () => null | Promise<Partial<ExpressionEntity[]>>
  buildStrates(defs: any): CreateStrateDTO[]
}
