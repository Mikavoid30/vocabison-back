import { Test, TestingModule } from '@nestjs/testing'
import { BisonService } from './bison.service'
import { LanguagesService } from '../core/languages/languages.service'
import { TranslatorService } from '../translator/translator.service'
import { ExpressionService } from '../expression/expression.service'
import { EntryService} from '../entry/entry.service'
import { StrateService } from '../strate/strate.service'
import { getRepositoryToken } from '@nestjs/typeorm'

import { UserEntity } from '../user/user.entity'
import { StrateEntity } from '../strate/strate.entity'
import { EntryEntity } from '../entry/entry.entity'
import { BoxEntity } from '../box/box.entity'
import { ExpressionEntity } from '../expression/expression.entity'
import { AutoCreateEntriesDTO } from '../entry/dto/auto-create-entry.dto'

const mockStrateRepo = {}
const mockEntryRepo = {}
const mockBoxRepo = {}
const mockExpressionRepo = {}
const mockUserRepo = {}

describe('BisonService', () => {
  let service: BisonService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BisonService,
        TranslatorService,
        LanguagesService,
        ExpressionService,
        EntryService,
        StrateService,
        {
          provide: getRepositoryToken(StrateEntity),
          useValue: mockStrateRepo,
        },
        {
          provide: getRepositoryToken(EntryEntity),
          useValue: mockEntryRepo,
        },
        {
          provide: getRepositoryToken(BoxEntity),
          useValue: mockBoxRepo,
        },
        {
          provide: getRepositoryToken(ExpressionEntity),
          useValue: mockExpressionRepo,
        },
        {
          provide: getRepositoryToken(UserEntity),
          useValue: mockUserRepo,
        },
      ]
    }).compile()

    service = module.get<BisonService>(BisonService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  describe('Initialization', () => {
    describe('When calling Init', () => {
      it('Should initialize the object, when input is valid', () => {
        const input: AutoCreateEntriesDTO = {
          expressions: ['hello', 'world'],
          source_lang: 'fr',
          destination_langs: ['en', 'es'],
          auto_translate: false,
          include_defs: false
        }

        service.init(input)
        expect(service.source_lang).toStrictEqual('FR')
        expect(service.destination_langs).toStrictEqual(['EN', 'ES'])
        expect(service.languages).toStrictEqual(['FR', 'EN', 'ES'])
        expect(service.terms).toStrictEqual(['hello', 'world'])
        expect(service.options.autoTranslate).toBe(false)
        expect(service.options.includeDefs).toBe(false)
      })

      it('Should throw an error if input is not valid', (done) => {
        const input: AutoCreateEntriesDTO = {
          expressions: ['hello'],
          source_lang: 'fr',
          destination_langs: ['abc', 'wrong'],
          auto_translate: false,
          include_defs: false
        }

        expect(() => service.init(input)).toThrowError('Destination languages are invalid')
        done()
      })

      it('Should have trimed every expressions and langs the empty ones', () => {
          const input: AutoCreateEntriesDTO = {
            expressions: ['hello      ', '         world     '],
            source_lang: '                           fr   ',
            destination_langs: ['en             ', ' es'],
            auto_translate: false,
            include_defs: false
          }

          service.init(input)
          expect(service.source_lang).toStrictEqual('FR')
          expect(service.destination_langs).toStrictEqual(['EN', 'ES'])
          expect(service.languages).toStrictEqual(['FR', 'EN', 'ES'])
          expect(service.terms).toStrictEqual(['hello', 'world'])
          expect(service.options.autoTranslate).toBe(false)
          expect(service.options.includeDefs).toBe(false)
        })
    })
  })

  describe('Autocreate', () => {
    describe('When wrong inputs', () => {
      const input: AutoCreateEntriesDTO = {
          expressions: [],
          source_lang: '',
          destination_langs: [],
          auto_translate: false,
          include_defs: false
      }

      it('Should send back null and never call any repository', async (done) => {
        const response = await service.autoCreateEntries(input, '1', '1')
        expect(response).toBe(null)
        done()
      })

    })
  })
})
