import { Module } from '@nestjs/common'
import { UserService } from '../user/user.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserEntity } from '../user/user.entity'
import { BisonService } from './bison.service'
import { CoreModule } from '../core/core.module'
import { EntryModule } from '../entry/entry.module'
import { TranslatorModule } from '../translator/translator.module'
import { StrateService } from '../strate/strate.service'
import { StrateModule } from '../strate/strate.module'

@Module({
  imports: [
    CoreModule,
    EntryModule,
    TranslatorModule,
    StrateModule,
    TypeOrmModule.forFeature([UserEntity])
  ],
  providers: [UserService, BisonService],
  exports: [BisonService]
})
export class BisonModule {}
