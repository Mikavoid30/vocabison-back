import { Injectable } from '@nestjs/common'
import { DicoBison } from 'dicobison'
import { IBison } from './bison.interface'
import { LanguagesService } from '../core/languages/languages.service'
import { ExpressionEntity } from '../expression/expression.entity'
import { AutoCreateEntriesDTO } from '../entry/dto/auto-create-entry.dto'
import { EntryService } from '../entry/entry.service'
import { ExpressionService } from '../expression/expression.service'
import { TranslatorService } from '../translator/translator.service'
import { EntryEntity } from '../entry/entry.entity'
import { StrateService } from '../strate/strate.service'
import { CreateStrateDTO } from '../strate/dto/create-strate.dto'
import { StrateEntity } from '../strate/strate.entity'

type Options = { includeDefs: boolean; autoTranslate: boolean }

type DicoBisonResponse = {
  originalExpression: any
  lang: string
  text: string
  type: string
  definitions: string[]
  examples: string[]
  synonyms: string[]
  printedWord?: string
}

type ExpressionWithDefs = {
  defs: DicoBisonResponse
  expression: ExpressionEntity
}

@Injectable()
export class BisonService implements IBison {
  terms: string[]
  source_lang: string
  destination_langs: string[]
  languages: string[]
  expressions: ExpressionEntity[]
  strates: StrateEntity[]
  options: Options

  constructor(
    private translatorService: TranslatorService,
    private languageService: LanguagesService,
    private expressionService: ExpressionService,
    private entryService: EntryService,
    private strateService: StrateService
  ) {
    this.expressions = []
    this.strates = []
  }

  /**
   *
   * @param input: AutoCreateEntriesDTO
   * Will initialize the BisonService with input provided
   */
  init(input: AutoCreateEntriesDTO) {
    this.validateInputOrFail(input)
    this.options = {
      autoTranslate: input.auto_translate || false,
      includeDefs: (input.auto_translate && input.include_defs) || false
    }

    this.terms = input.expressions
      .map((term: string) => term.trim())
      .filter((term: string) => term && term.length)

    this.source_lang = input.source_lang.toUpperCase().trim()
    this.destination_langs = input.destination_langs.map((x) =>
      x.toUpperCase().trim()
    )
    this.languages = [
      ...new Set(
        [this.source_lang, ...this.destination_langs].map((x) =>
          x.toUpperCase()
        )
      )
    ]
  }

  async autoCreateEntries(
    input: AutoCreateEntriesDTO,
    boxId: string,
    userId: string
  ): Promise<EntryEntity[]> {
    try {
      this.init(input)
      const translatedExpressions: ExpressionEntity[][] = await this.buildExpressions(
        input.auto_translate
      )

      // Create every expressions
      const ents: Promise<EntryEntity>[] = translatedExpressions.map(
        (expressions: ExpressionEntity[]) => {
          return this.buildEntry(expressions, boxId, userId)
        }
      )

      const entries = await Promise.all(ents)

      await this.addStrates(entries, userId, input.include_defs)
      return entries
    } catch (e) {
      process.env.NODE_ENV === 'dev' && console.error(e.message)
      return null
    }
  }

  async addStrates(
    entries: EntryEntity[],
    userId: string,
    includeDefs: boolean = this.options.includeDefs
  ) {
    const db = new DicoBison({
      fakeScraping: process.env.DO_FAKE_SCRAPING === 'true'
    })
    const proms: Promise<any>[] = entries.map(async (entry: EntryEntity) => {
      let expressions = this.expressions.filter(
        (e: ExpressionEntity) => e.entry.id === entry.id
      )
      const withDefs: DicoBisonResponse[] = includeDefs
        ? ((await db.scrapData(expressions as any)) as any)
        : await this.emptyDefs(expressions)

      return withDefs.map(
        ({ definitions, examples, synonyms, originalExpression }) => {
          return {
            expression: originalExpression,
            defs: {
              definitions: definitions || [],
              examples: examples || [],
              synonyms: synonyms || []
            }
          }
        }
      )
    })

    const completeExpressions = await Promise.all(proms)
    completeExpressions.forEach((entryExpressions: ExpressionWithDefs[]) => {
      entryExpressions.forEach(
        async ({ defs, expression }: ExpressionWithDefs) => {
          const strates = await this.buildStrates(defs)
          strates.forEach((strate: CreateStrateDTO) => {
            this.strateService.createStrate(
              expression.entry.id,
              expression.id,
              userId,
              {
                ...strate
              }
            )
          })
        }
      )
    })
  }

  async buildEntry(
    expressions: ExpressionEntity[],
    boxId: string,
    userId: string
  ): Promise<EntryEntity> {
    const entry = await this.entryService.createEntry(
      { comment: '' },
      boxId,
      userId
    )
    const expressionsProms = expressions.map((expression: ExpressionEntity) => {
      return this.expressionService.createExpression(
        boxId,
        entry.id,
        userId,
        expression
      )
    })
    const ex = await Promise.all(expressionsProms)
    this.expressions.push(...ex)
    return entry
  }

  async buildExpressions(
    autoTranslate: boolean = this.options.autoTranslate
  ): Promise<any[]> {
    const expressions = []

    const translations = autoTranslate
      ? await this.getTranslations()
      : await this.getEmptyTranslations()

    const len = translations.length
    for (let i = 0; i < len; i++) {
      expressions.push(
        translations[i].map((translated: any) => {
          const ex = new ExpressionEntity()
          return { ...ex, ...translated }
        })
      )
    }

    return expressions
  }

  buildStrates(defs: any = []): CreateStrateDTO[] {
    const { definitions = [], synonyms = [], examples = [] } = defs
    return [
      { items: definitions, template: 'TEXTLIST', type: 'DEFINITIONS' },
      { items: synonyms, template: 'TAGS', type: 'SYNONYMS' },
      { items: examples, template: 'TEXTLIST', type: 'EXAMPLES' },
      { items: [''], template: 'TEXT', type: 'COMMENTS' }
    ]
  }

  async emptyDefs(expressions: ExpressionEntity[]) {
    return expressions.map((exp: ExpressionEntity) => {
      return {
        originalExpression: exp,
        definitions: [],
        examples: [],
        synonyms: [],
        text: '',
        lang: ''
      }
    })
  }

  async getTranslations(): Promise<any[]> {
    const from = this.source_lang
    const proms = this.terms.map((src_text: string) => {
      return this.translatorService.getTranslations(
        from,
        src_text,
        this.destination_langs
      )
    })
    const translations = await Promise.all(proms)
    return translations
  }

  getEmptyTranslations(): any {
    const langs = [...this.destination_langs, this.source_lang]
    return this.terms.map((src_text: string) => {
      return langs.map((lang: string) => {
        return {
          text: lang === this.source_lang ? src_text : '',
          lang
        }
      })
    })
  }

  validateInputOrFail(input: AutoCreateEntriesDTO) {
    if (
      !input.destination_langs ||
      !input.destination_langs.length ||
      !input.destination_langs.every(
        (x: string) =>
          typeof x === 'string' && this.languageService.isValidCode(x.trim())
      )
    ) {
      throw new Error('Destination languages are invalid')
    }

    if (!this.languageService.isValidCode(input.source_lang.trim())) {
      throw new Error('Source language invalid')
    }

    if (
      !input.expressions ||
      !input.expressions.length ||
      !input.expressions.every((x) => typeof x === 'string')
    ) {
      throw new Error('Expressions are invalid')
    }
  }
}
