import { Resolver, Query, Mutation, Args, Context } from '@nestjs/graphql'
import { UserService } from './user.service'
import { MyContext } from '../common/interfaces/mycontext.interface'
import { MailerService } from '../core/mailer/mailer.service'
import { redis } from '../common/redis/redis'
import { SetMetadata, UseGuards, Logger } from '@nestjs/common'
import { Authorized } from '../common/guards/authorized.guard'
import { roles } from '../common/enums/roles.enum'
import { RegisterUserDTO } from './dto/register-user.dto'
import { LoginUserDTO } from './dto/login-user.dto'
import { ConfirmUserDTO } from './dto/confirm-user.dto'
import { ForgotPasswordDTO } from './dto/forgot-password.dto'
import { User } from '../graphql.schema'
import { ChangePasswordDTO } from './dto/change-password.dto'
import {
  forgotPasswordPrefix,
  confirmationPrefix
} from '../common/redis/redis.prefixes'

@Resolver('User')
export class UserResolver {
  constructor(
    private readonly userService: UserService,
    private readonly mailerService: MailerService
  ) {}

  // Public Queries
  @Query('me')
  async me(@Context() context: MyContext): Promise<User | null> {
    const id = context.req.session.userId
    if (!id) return null
    return await this.userService.findOneById(id)
  }

  // Public Mutations
  @Mutation('register')
  async register(@Args('registerInput')
  {
    email,
    password,
    native_language
  }: RegisterUserDTO): Promise<User> {
    const user = await this.userService.register(
      email,
      password,
      native_language
    )
    if (user) {
      this.mailerService.sendConfirmationEmail(
        email,
        user.createConfirmationUrl(confirmationPrefix, 'confirm')
      )
    }
    return user
  }

  @Mutation('login')
  async login(
    @Args('loginInput') { email, password }: LoginUserDTO,
    @Context() context: MyContext
  ): Promise<User> {
    const user = await this.userService.findByCredentials(email, password)
    if (!user) return null
    context.req.session.userId = user.id
    return user
  }

  @Mutation('logout')
  async logout(@Context() ctx: MyContext): Promise<boolean> {
    return new Promise((resolve, reject) => {
      ctx.req.session.destroy((err) => {
        if (err) {
          Logger.error(err.message || 'Error on logout', null, 'UserResolver')
          return reject(false)
        }
        ctx.res.clearCookie('qid')
        return resolve(true)
      })
    })
  }

  @Mutation('confirm')
  async confirm(@Args() { token }: ConfirmUserDTO): Promise<boolean> {
    const key = `${confirmationPrefix}:${token}`
    const userId = await redis.get(key)
    if (!userId) return false
    const confirmed = await this.userService.confirm(userId)
    redis.del(key)
    return confirmed
  }

  @Mutation('forgotPassword')
  async forgotPassword(@Args() { email }: ForgotPasswordDTO): Promise<boolean> {
    const user = await this.userService.findOneByEmail(email)
    if (user) {
      this.mailerService.sendForgotPasswordEmail(
        user.email,
        user.createConfirmationUrl(forgotPasswordPrefix, 'forgot-password')
      )
    }
    return true
  }

  @Mutation('changePassword')
  async changePassword(
    @Args() { token, password }: ChangePasswordDTO,
    @Context() ctx: any
  ): Promise<boolean> {
    const key = `${forgotPasswordPrefix}:${token}`
    const userId = await redis.get(key)
    if (!userId) return false
    const confirmed = await this.userService.changePassword(userId, password)
    ctx.req.session.userId = userId /* Log in the user */
    redis.del(key)
    return confirmed
  }

  // Private Queries
  @Query('users')
  @SetMetadata('roles', [roles.ADMIN])
  @UseGuards(Authorized)
  users() {
    return this.userService.showAll()
  }

  // Private Mutations
  @Mutation('ban')
  @SetMetadata('roles', [roles.ADMIN])
  @UseGuards(Authorized)
  async ban(@Args('id') userId: string): Promise<string | null> {
    return this.userService.setBan(userId, true)
  }

  @Mutation('unban')
  @SetMetadata('roles', [roles.ADMIN])
  @UseGuards(Authorized)
  async unban(@Args('id') userId: string): Promise<string | null> {
    return this.userService.setBan(userId, false)
  }
}
