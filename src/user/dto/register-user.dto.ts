import { IsString, IsEmail, MinLength, MaxLength } from 'class-validator'
import { RegisterUserInput } from '../../graphql.schema'

export class RegisterUserDTO extends RegisterUserInput {
  @IsString()
  @IsEmail()
  @MinLength(5)
  readonly email: string

  @IsString()
  @MinLength(2)
  @MaxLength(2)
  readonly native_language: string

  @IsString()
  @MinLength(+process.env.PASSWORD_MIN_LENGTH || 6)
  password: string
}
