import { IsString, MinLength } from 'class-validator'

export class ChangePasswordDTO {
  @IsString()
  @MinLength(10)
  readonly token: string

  @IsString()
  @MinLength(+process.env.PASSWORD_MIN_LENGTH || 6)
  readonly password: string
}
