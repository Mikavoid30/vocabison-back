import { IsString, MinLength, IsEmail } from 'class-validator'

export class ForgotPasswordDTO {
  @IsString()
  @IsEmail()
  @MinLength(5)
  readonly email: string
}
