import { LoginUserInput } from './../../graphql.schema'
import { IsString, IsEmail, MinLength } from 'class-validator'

export class LoginUserDTO extends LoginUserInput {
  @IsString()
  @IsEmail()
  @MinLength(5)
  readonly email: string

  @IsString()
  password: string
}
