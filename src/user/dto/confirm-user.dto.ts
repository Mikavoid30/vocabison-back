import { IsString, MinLength } from 'class-validator'

export class ConfirmUserDTO {
  @IsString()
  @MinLength(10)
  readonly token: string
}
