import {
  Injectable,
  BadRequestException,
  NotFoundException
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { UserEntity } from './user.entity'
import { Repository } from 'typeorm'
import { User } from '../graphql.schema'
import { roles } from '../common/enums/roles.enum'

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity) private userRepository: Repository<UserEntity>
  ) {}

  async showAll(): Promise<User[]> {
    return await this.userRepository.find()
  }

  async register(
    email: string,
    password: string,
    native_language: string
  ): Promise<UserEntity> {
    const user = await this.userRepository.findOne({
      where: { email }
    })
    if (user) throw new BadRequestException('User already exists')
    const newUser = await this.userRepository.create({
      email,
      password,
      native_language
    })
    await this.userRepository.save(newUser)
    return newUser
  }

  async findByCredentials(
    email: string,
    password: string
  ): Promise<UserEntity> {
    const user = await this.userRepository.findOne({
      where: { email }
    })
    if (!user || !(await user.comparePassword(password)) || user.banned) {
      return null
    }

    if (!user.confirmed) {
      throw new Error('Account not confirmed')
    }
    return user
  }

  async findOneById(userId: string): Promise<UserEntity> {
    const user = await this.userRepository.findOne({
      where: { id: userId }
    })
    if (!user) {
      throw new NotFoundException()
    }
    return user
  }

  async findOneByEmail(email: string): Promise<UserEntity> {
    const user = await this.userRepository.findOne({
      where: { email }
    })
    if (!user) {
      throw new NotFoundException()
    }
    return user
  }

  async confirm(userId: string): Promise<boolean> {
    try {
      await this.userRepository.update({ id: userId }, { confirmed: true })
      return true
    } catch (e) {
      return false
    }
  }

  async changePassword(userId: string, password: string): Promise<boolean> {
    try {
      const user = await this.userRepository.findOne({ where: { id: userId } })
      user.password = password
      await this.userRepository.save(user)
      return true
    } catch (e) {
      return false
    }
  }

  async setBan(userId: string, banned: boolean): Promise<string | null> {
    try {
      await this.userRepository.update({ id: userId }, { banned })
      return userId
    } catch (e) {
      return null
    }
  }

  // Not exposed in Resolvers
  async setRoles(userId: string, newRoles: roles[]): Promise<string | null> {
    try {
      const validRoles = [roles.ADMIN, roles.PRO, roles.USER]
      if (!newRoles.every((x) => validRoles.includes(x)))
        throw new Error('One role is not valid')
      await this.userRepository.update({ id: userId }, { roles: newRoles })
      return userId
    } catch (e) {
      return null
    }
  }
}
