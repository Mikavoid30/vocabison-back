import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserEntity } from './user.entity'
import { UserResolver } from './user.resolver'
import { UserService } from './user.service'
import { CoreModule } from '../core/core.module'
import { DateScalar } from '../common/graphql/scalars/date.scalar'

@Module({
  imports: [CoreModule, TypeOrmModule.forFeature([UserEntity])],
  controllers: [],
  providers: [UserResolver, UserService, DateScalar],
  exports: [UserService]
})
export class UserModule {}
