import bcrypt from 'bcryptjs'
import { v4 } from 'uuid'
import { redis } from '../common/redis/redis'

import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  BeforeInsert,
  BeforeUpdate,
  OneToMany
} from 'typeorm'
import { ForbiddenException } from '@nestjs/common'
import { roles } from '../common/enums/roles.enum'
import { BoxEntity } from '../box/box.entity'
import { FicheEntity } from '../fiche/fiche.entity'

@Entity('user')
export class UserEntity {
  @PrimaryGeneratedColumn() id: string
  @CreateDateColumn() created: Date
  @UpdateDateColumn() updated: Date

  @Column('text', { unique: true }) email: string
  @Column() password: string
  @Column() native_language: string
  @Column({
    type: 'enum',
    enum: roles,
    array: true,
    default: [roles.USER]
  })
  roles: roles[]

  @OneToMany((type) => BoxEntity, (box) => box.owner)
  boxes: BoxEntity[]
  @OneToMany((type) => FicheEntity, (fiche) => fiche.owner)
  fiches: FicheEntity[]
  @Column('bool', { default: false }) confirmed: boolean
  @Column('bool', { default: false }) banned: boolean

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    try {
      this.password = await bcrypt.hash(this.password, 8)
    } catch (e) {
      throw new ForbiddenException('Malformed password')
    }
    return true
  }

  async comparePassword(attempt: string) {
    return await bcrypt.compare(attempt, this.password)
  }

  createConfirmationUrl(prefix: string, path: string = prefix) {
    const isTest = process.env.NODE_ENV === 'test'
    const token = isTest ? this.email.toLowerCase() : v4()
    const exp = isTest ? 60 : 60 * 60 * 24 * 7
    redis.set(`${prefix}:${token}`, this.id, 'ex', exp) // 7d expiration

    return `${process.env.APP_URL}/user/${path}/${token}`
  }
}
