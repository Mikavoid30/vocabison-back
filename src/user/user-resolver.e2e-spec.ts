import { Test } from '@nestjs/testing'
import sessionRequest from 'supertest-session'
import dotenv from 'dotenv'
import { INestApplication } from '@nestjs/common'
import { GraphQLModule } from '@nestjs/graphql'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigModule, ConfigService } from 'nestjs-config'
import { UserModule } from './user.module'
import path from 'path'
import session from 'express-session'
import { Helper, SESSION_OPTIONS } from '../../test/utils/helper.utils'
import { RegisterUserDTO } from './dto/register-user.dto'
import { APP_PIPE } from '@nestjs/core'
import { ValidationPipe } from '../common/pipes/validation.pipe'

describe('Auth GraphQL (e2e)', () => {
  let app: INestApplication
  let helper: Helper

  beforeAll(async () => {
    dotenv.config({ path: `.test.env` })
    const module = await Test.createTestingModule({
      imports: [
        ConfigModule.load(
          path.resolve(
            __dirname,
            '..',
            '..',
            'test',
            'config',
            '**/!(*.d).{ts,js}'
          )
        ),
        GraphQLModule.forRootAsync({
          useFactory: () => ({
            typePaths: ['./**/*.graphql'],
            context: ({ req, res }) => ({ req, res })
          })
        }),
        TypeOrmModule.forRootAsync({
          useFactory: (config: ConfigService) => config.get('database'),
          inject: [ConfigService]
        }),
        UserModule
      ],
      providers: [
        {
          provide: APP_PIPE,
          useClass: ValidationPipe
        }
      ]
    }).compile()

    app = module.createNestApplication()
    helper = new Helper(sessionRequest(app.getHttpServer()))
    app.use(session(SESSION_OPTIONS))
    await app.init()
  })

  describe('Authentication flow', () => {
    let testUser: RegisterUserDTO
    describe('Me query', () => {
      it('should detect that we are not logged in', async () => {
        const response = await helper.runGql(`{ me { id } }`)
        expect(response.body).toStrictEqual({ data: { me: null } })
      })
    })

    describe('Register Mutation', async () => {
      it('should create a user', async () => {
        testUser = await helper.getFakeUserToRegister()
        const response = await helper.runRegister(testUser)
        const { register } = response.body.data
        expect(register.id).toBeTruthy()
        expect(register.email).toStrictEqual(testUser.email)
        expect(register.created).toBeTruthy()
        expect(register.native_language).toStrictEqual('FR')
      })

      it('should give me errors on duplicate user', async () => {
        const response = await helper.runRegister(testUser)
        const {
          data: { register },
          errors: [{ message }]
        } = response.body
        expect(register).toBeFalsy()
        expect(message.message).toStrictEqual('User already exists')
        expect(message.statusCode).toStrictEqual(400)
      })

      it('should give me errors on wrong inputs', async () => {
        const response = await helper.runRegister({
          email: 'notanemail',
          password: 'short',
          native_language: 'TOOLONG'
        })
        const {
          data: { register },
          errors: [{ message }]
        } = response.body
        expect(register).toBeFalsy()
        expect(message.message[0]).toStrictEqual({
          property: 'email',
          isEmail: 'email must be an email'
        })
        expect(message.message[1]).toStrictEqual({
          property: 'native_language',
          maxLength:
            'native_language must be shorter than or equal to 2 characters'
        })
        expect(message.message[2]).toStrictEqual({
          property: 'password',
          minLength: 'password must be longer than or equal to 6 characters'
        })
      })
    })

    describe('Login Mutation', async () => {
      it('should login a user but not confirm it', async () => {
        const response = await helper.runLogin(testUser)
        const { errors, data } = response.body
        expect(data.login).toStrictEqual(null)
        expect(errors.length).toBeGreaterThan(0)
        expect(errors[0].message).toStrictEqual('Account not confirmed')
      })

      it('should not log me in with a wrong password', async () => {
        const response = await helper.runLogin({
          ...testUser,
          password: 'wrong-password'
        })
        const { errors, data } = response.body
        expect(errors).toBeFalsy()
        expect(data.login).toStrictEqual(null)
      })
    })

    describe('Confirm Mutation', async () => {
      it('should give an error if no token', async () => {
        const response = await helper.runConfirm({ token: null })
        const {
          errors: [{ message }],
          data
        } = response.body
        expect(message.message[0]).toStrictEqual({
          minLength: 'token must be longer than or equal to 10 characters',
          property: 'token'
        })
        expect(data).toStrictEqual({ confirm: null })
      })

      it('should give false with a wrong token', async () => {
        const response = await helper.runConfirm({ token: 'azertyuiopq' })
        const { data } = response.body
        expect(data).toStrictEqual({ confirm: false })
      })

      it('should confirm the user and allow him to login', async () => {
        // In test env, token are lowercased user email
        const response = await helper.runConfirm({
          token: testUser.email.toLowerCase()
        })
        const { data } = response.body
        expect(data).toStrictEqual({ confirm: true })
        // Login
        const loginResponse = await helper.runLogin(testUser)
        const {
          errors,
          data: { login }
        } = loginResponse.body
        expect(login.email).toStrictEqual(testUser.email)
        expect(login.id).toStrictEqual('1')
        expect(login.created).toBeGreaterThan(1000000000)
        expect(errors).toBeFalsy()
      })

      it('should detect that we are logged in', async () => {
        const response = await helper.runGql(`{ me { id } }`)
        expect(response.body).toStrictEqual({
          data: { me: { id: '1' } }
        })
      })
    })

    describe('Forgot / Change password', () => {
      // Forgot password
      it('should init a forgot password', async () => {
        const response = await helper.runGql(`
        mutation {
          forgotPassword(email: "${testUser.email}")
        }`)
        expect(response.body).toStrictEqual({
          data: {
            forgotPassword: true
          }
        })
      })

      it('should not init a forgot password on wrong email', async () => {
        const response = await helper.runGql(`
        mutation {
          forgotPassword(email: "wrong@email.fr")
        }`)
        const {
          errors: [{ message }],
          data
        } = response.body
        expect(message.error).toStrictEqual('Not Found')
        expect(data).toStrictEqual({ forgotPassword: null })
      })

      it("should not change the user's password if wrong token", async () => {
        const response = await helper.runGql(`
        mutation { 
          changePassword(token: "abcfefghijklmnop", password: "newpassword")
        }`)
        expect(response.body).toMatchObject({
          data: {
            changePassword: false
          }
        })
      })

      it('should throw an error if no token', async () => {
        const response = await helper.runGql(`
        mutation { 
          changePassword(token: "", password: "newpassword")
        }`)
        const {
          errors: [{ message }],
          data
        } = response.body
        expect(message.message[0]).toMatchObject({
          minLength: 'token must be longer than or equal to 10 characters',
          property: 'token'
        })
        expect(data).toStrictEqual({ changePassword: null })
      })

      it("should change the user's password", async () => {
        const response = await helper.runGql(`
        mutation { 
          changePassword(token: "${testUser.email.toLowerCase()}", password: "newpassword")
        }`)
        expect(response.body).toStrictEqual({
          data: {
            changePassword: true
          }
        })
      })

      it('should not log me in with the old password', async () => {
        const loginResponse = await helper.runLogin(testUser)
        const {
          data: { login }
        } = loginResponse.body
        expect(login).toStrictEqual(null)
      })

      it('should log me in with the new password', async () => {
        testUser.password = 'newpassword'
        const loginResponse = await helper.runLogin(testUser)
        const {
          data: { login }
        } = loginResponse.body
        expect(login.id).toStrictEqual('1')
        const meResponse = await helper.runGql(`{ me { id } }`)
        expect(meResponse.body).toStrictEqual({ data: { me: { id: '1' } } })
      })
    })

    describe('Logout', () => {
      it('should log me out', async () => {
        const logoutResponse = await helper.runGql(`mutation { logout }`)
        expect(logoutResponse.body).toStrictEqual({
          data: { logout: true }
        })
        const meResponse = await helper.runGql(`{ me { id } }`)
        expect(meResponse.body).toStrictEqual({
          data: { me: null }
        })
      })
    })
  })
})
