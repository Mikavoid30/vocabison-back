import { Resolver, Query, Args, Context, Mutation } from '@nestjs/graphql'
import { SetMetadata, UseGuards } from '@nestjs/common'
import { roles } from '../common/enums/roles.enum'
import { Authorized } from '../common/guards/authorized.guard'
import { FicheEntity } from './fiche.entity'
import { FicheService } from './fiche.service'
import { CreateFicheDTO } from './dto/createFiche.dto'
import { UpdateFicheDTO } from './dto/updateFiche.dto'

@Resolver('fiche')
export class FicheResolver {
  constructor(private ficheService: FicheService) {}

  @Query('fiche')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  fiche(
    @Args('ficheId') ficheId: string,
    @Context() ctx: any
  ): Promise<FicheEntity> {
    return this.ficheService.getOneFiche(ficheId, ctx.req.session.userId)
  }

  @Query('fiches')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  fiches(@Context() ctx: any): Promise<FicheEntity[]> {
    return this.ficheService.getAllFichesFromUser(ctx.req.session.userId)
  }

  @Mutation('createFiche')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  createFiche(
    @Args('createFicheInput') input: CreateFicheDTO,
    @Context() ctx: any
  ) {
    return this.ficheService.createFiche(ctx.req.session.userId, input)
  }

  @Mutation('updateFiche')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  updateFiche(
    @Args('ficheId') ficheId: string,
    @Args('updateFicheInput') input: UpdateFicheDTO,
    @Context() ctx: any
  ) {
    return this.ficheService.updateFiche(ctx.req.session.userId, ficheId, input)
  }
}
