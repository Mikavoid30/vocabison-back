import { IsString, IsOptional } from 'class-validator'
import { UpdateFicheInput } from '../../graphql.schema'

export class UpdateFicheDTO extends UpdateFicheInput {
  @IsString()
  @IsOptional()
  readonly title: string

  @IsString()
  @IsOptional()
  readonly boxId: string
}
