import { IsString, IsBoolean, IsNumber, IsOptional } from 'class-validator'
import { CreateFicheInput } from '../../graphql.schema'

export class CreateFicheDTO extends CreateFicheInput {
  @IsString({ each: true })
  readonly visibleLanguages: string[]

  @IsString()
  readonly boxId: string

  @IsString()
  readonly title: string

  @IsString()
  @IsOptional()
  readonly date: string

  @IsBoolean()
  @IsOptional()
  readonly showComment: boolean

  @IsBoolean()
  @IsOptional()
  readonly showType: boolean

  @IsNumber()
  @IsOptional()
  readonly nbDefs: number

  @IsNumber()
  @IsOptional()
  readonly nbExamples: number

  @IsNumber()
  @IsOptional()
  readonly nbSynonyms: number

  @IsNumber()
  @IsOptional()
  readonly nbTranslations: number

  @IsNumber()
  @IsOptional()
  readonly nbMaxEntries: number
}
