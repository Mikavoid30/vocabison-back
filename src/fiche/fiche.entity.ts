import { UserEntity } from '../user/user.entity'
import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne
} from 'typeorm'

@Entity('fiche')
export class FicheEntity {
  @PrimaryGeneratedColumn() id: string
  @CreateDateColumn() created: Date
  @UpdateDateColumn() updated: Date

  @Column({ default: null }) boxId: string
  @Column('simple-array', { default: null, nullable: true })
  visibleLanguages: string[]
  @Column('text') title: string
  @Column('text', { default: new Date().toLocaleDateString() }) date: string
  @Column('text', { default: null, nullable: true }) bisonFileToken: string
  @Column({ default: true }) showComment: boolean
  @Column({ default: true }) showType: boolean
  @Column({ default: 0 }) nbDefs: number
  @Column({ default: 0 }) nbExamples: number
  @Column({ default: 0 }) nbSynonyms: number
  @Column({ default: 1 }) nbTranslations: number
  @Column({ default: 0 }) nbMaxEntries: number // 0 means no limit

  @ManyToOne((type) => UserEntity, (user) => user.fiches)
  owner: UserEntity
}
