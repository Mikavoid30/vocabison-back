import { Injectable, NotFoundException } from '@nestjs/common'
import { FicheEntity } from './fiche.entity'
import { Repository } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'
import { UserEntity } from '../user/user.entity'
import { CreateFicheDTO } from './dto/createFiche.dto'
import { UpdateFicheDTO } from './dto/updateFiche.dto'
import { EntryEntity } from 'src/entry/entry.entity'
import { BoxEntity } from '../box/box.entity'
import { ExpressionEntity } from 'src/expression/expression.entity'
import { StrateEntity } from 'src/strate/strate.entity'
import { STRATE_TYPES } from '../strate/strate.entity'
import { LanguagesService } from '../core/languages/languages.service'
import { FileManagerService } from '../file-manager/file-manager.service'

@Injectable()
export class FicheService {
  constructor(
    private languageService: LanguagesService,
    private fileManagerService: FileManagerService,
    @InjectRepository(FicheEntity)
    private ficheRepository: Repository<FicheEntity>,
    @InjectRepository(EntryEntity)
    private entryRepository: Repository<EntryEntity>,
    @InjectRepository(BoxEntity)
    private boxRepository: Repository<BoxEntity>,
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity> // dyn called
  ) {}

  async getAllFichesFromUser(userId: string) {
    try {
      const owner = await this._get('user', userId)
      const fiches = await this.ficheRepository.find({
        where: { owner }
      })
      if (!owner || !fiches) throw new NotFoundException(`Cannot find fiches`)
      return fiches
    } catch (e) {
      console.error(e)
      throw new NotFoundException()
    }
  }

  async getOneFiche(ficheId: string, userId: string) {
    try {
      const owner = await this._get('user', userId)
      const fiche = await this.ficheRepository.findOne({
        where: { id: ficheId, owner }
      })
      if (!owner || !fiche)
        throw new NotFoundException(`Cannot find fiche n°${ficheId}`)
      return fiche
    } catch (e) {
      console.error(e)
      throw new NotFoundException()
    }
  }

  async createFiche(userId: string, input: CreateFicheDTO) {
    const owner = await this._get('user', userId)
    let bisonFileToken = null
    let existingFiche = null

    try {
      // For now we only use one fiche by box
      existingFiche = await this.ficheRepository.findOne({
        where: { boxId: input.boxId, owner }
      })
      const bisonData = await this._getBisonData(input, owner)
      bisonFileToken = await this.fileManagerService.saveBisonFile(
        bisonData as any,
        existingFiche && existingFiche.bisonFileToken
      )
    } catch (e) {
      throw new Error('Cannot create fiche for those lists')
    }
    const fiche =
      existingFiche ||
      this.ficheRepository.create({
        ...input,
        bisonFileToken,
        owner
      })
    const savedFiche = await this.ficheRepository.save(fiche)
    return savedFiche
  }

  async updateFiche(userId: string, ficheId: string, input: UpdateFicheDTO) {
    const owner = await this._get('user', userId)
    const fiche = await this.ficheRepository.findOne({
      where: { id: ficheId, owner }
    })
    if (!owner || !fiche)
      throw new NotFoundException(`Cannot find fiche n°${ficheId}`)
    const updated = { ...fiche, ...input }
    await this.ficheRepository.save(updated)
    return updated
  }

  private async _get(model: string, id: string, relations?: string[]) {
    try {
      const repo = this[`${model.toLowerCase()}Repository`]
      if (!repo) throw new Error(`Repo for ${model} does not exist`)
      return await repo.findOneOrFail({
        where: { id },
        relations
      })
    } catch (e) {
      throw new NotFoundException(model + ' not found')
    }
  }

  private async _getBisonData(options: CreateFicheDTO, user: UserEntity) {
    // make sure boxIds exists and are owned by the user
    const ownerBox = await this.boxRepository.findOne({
      where: {
        owner: user,
        id: options.boxId
      }
    })
    if (!ownerBox) throw new NotFoundException('Cannot find this list')

    const entries = await this.entryRepository.find({
      where: {
        box: ownerBox.id
      },
      relations: ['expressions', 'box', 'expressions.strates'],
      order: {
        created: 'DESC'
      },
      take: options.nbMaxEntries === 0 ? 10e6 : options.nbMaxEntries
    })

    const metaObject = this._buildMetaObject(options, user, ownerBox)
    const formattedEntries = this._buildEntries(entries, options)

    return {
      meta: { ...metaObject },
      entries: formattedEntries
    }
  }

  private _buildMetaObject(
    options: CreateFicheDTO,
    owner: UserEntity,
    box: BoxEntity
  ) {
    let { title = 'Untitled', date = new Date().toLocaleDateString() } = options
    const allowedLangs = options.visibleLanguages.length
      ? options.visibleLanguages.filter((x) => box.languages.includes(x))
      : box.languages
    const langs = [...new Set([...allowedLangs])].map((x) => {
      return {
        code: x.toLowerCase(),
        text: this.languageService.getLangFromCode(x)
      }
    })
    return {
      title,
      date,
      author: owner.email,
      isPro: true,
      tagline: langs.map((x) => x.text).join(', '),
      langs: langs,
      nbLangs: langs.length
    }
  }

  private _buildEntries(entries: EntryEntity[], options: CreateFicheDTO) {
    /** @todo onther translations  */
    return entries.map((x) => {
      return {
        exps: x.expressions.map((ex: ExpressionEntity) => {
          const {
            showComment = false,
            nbSynonyms = 0,
            nbExamples = 0,
            nbDefs = 0
          } = options
          const commentStrate: StrateEntity =
            showComment &&
            ex.strates.find(
              (s: StrateEntity) => s.type === STRATE_TYPES.COMMENTS
            )
          const synonymsStrate: StrateEntity =
            nbSynonyms > 0 &&
            ex.strates.find(
              (s: StrateEntity) => s.type === STRATE_TYPES.SYNONYMS
            )
          const examplesStrate: StrateEntity =
            nbExamples > 0 &&
            ex.strates.find(
              (s: StrateEntity) => s.type === STRATE_TYPES.EXAMPLES
            )
          const definitionsStrate: StrateEntity =
            nbDefs > 0 &&
            ex.strates.find(
              (s: StrateEntity) => s.type === STRATE_TYPES.DEFINITIONS
            )
          return {
            w: ex.text,
            t: '',
            o: [],
            l: ex.lang,
            c: commentStrate ? commentStrate.items[0] : '',
            s: synonymsStrate ? synonymsStrate.items.slice(0, nbSynonyms) : [],
            e: examplesStrate ? examplesStrate.items.slice(0, nbExamples) : [],
            d: definitionsStrate ? definitionsStrate.items.slice(0, nbDefs) : []
          }
        })
      }
    })
  }
}
