import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { FicheEntity } from './fiche.entity'
import { UserEntity } from '../user/user.entity'
import { FicheResolver } from './fiche.resolver'
import { FicheService } from './fiche.service'
import { UserService } from '../user/user.service'
import { EntryEntity } from '../entry/entry.entity'
import { BoxEntity } from '../box/box.entity'
import { CoreModule } from '../core/core.module'
import { FileManagerModule } from '../file-manager/file-manager.module'

@Module({
  imports: [
    CoreModule,
    FileManagerModule,
    TypeOrmModule.forFeature([FicheEntity, BoxEntity, EntryEntity, UserEntity])
  ],
  providers: [FicheService, UserService, FicheResolver],
  exports: [FicheService]
})
export class FicheModule {}
