import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne,
  OneToMany
} from 'typeorm'
import { EntryEntity } from '../entry/entry.entity'
import { StrateEntity } from '../strate/strate.entity'

@Entity('expression')
export class ExpressionEntity {
  @PrimaryGeneratedColumn() id: string
  @CreateDateColumn() created: Date
  @UpdateDateColumn() updated: Date

  @Column('text') text: string
  @Column('text') lang: string

  @ManyToOne((type) => EntryEntity, (entry) => entry.expressions, {
    onDelete: 'CASCADE'
  })
  entry: EntryEntity

  @OneToMany((type) => StrateEntity, (strate) => strate.expression)
  strates: StrateEntity[]
}
