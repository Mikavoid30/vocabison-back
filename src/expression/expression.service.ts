import { Injectable, NotFoundException } from '@nestjs/common'
import { EntryEntity } from '../entry/entry.entity'
import { Repository } from 'typeorm'
import { ExpressionEntity } from './expression.entity'
import { InjectRepository } from '@nestjs/typeorm'
import { BoxEntity } from '../box/box.entity'
import { UpdateExpressionDTO } from './dto/update-expression.dto'
import { CreateExpressionDTO } from './dto/create-expression.dto'
import { StrateEntity } from '../strate/strate.entity'
import { UserEntity } from '../user/user.entity'

@Injectable()
export class ExpressionService {
  constructor(
    @InjectRepository(StrateEntity)
    private strateRepository: Repository<StrateEntity>,
    @InjectRepository(EntryEntity)
    private entryRepository: Repository<EntryEntity>,
    @InjectRepository(BoxEntity)
    private boxRepository: Repository<BoxEntity>,
    @InjectRepository(ExpressionEntity)
    private expressionRepository: Repository<ExpressionEntity>,
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>
  ) {}

  async getAllExpressionFromEntry(
    entryId: string,
    userId: string,
    n: number,
    p: number
  ) {
    const entry = await this.entryRepository.findOne({
      where: { id: entryId },
      relations: ['box', 'box.owner', 'expressions', 'expressions.strates']
    })

    if (!entry || entry.box.owner.id !== userId)
      throw new NotFoundException(`Cannot find entry n°${entryId}`)

    return entry && entry.expressions ? entry.expressions : []
  }

  async findExpression(
    entryId: string,
    expressionId: string,
    userId: string
  ): Promise<ExpressionEntity> {
    const entry = await this.entryRepository.findOne({
      where: { id: entryId },
      relations: ['box', 'box.owner']
    })
    if (!entry || entry.box.owner.id !== userId)
      throw new NotFoundException(`Cannot find entry n°${entryId}`)

    const expression = await this.expressionRepository.findOne({
      where: { id: expressionId },
      relations: ['entry', 'strates']
    })

    if (!expression || expression.entry.id !== entry.id) {
      throw new NotFoundException(`Cannot find expression n°${expressionId}`)
    }

    return expression
  }

  async createExpression(
    boxId: string,
    entryId: string,
    userId: string,
    input: CreateExpressionDTO
  ) {
    const owner = await this.get('user', userId)
    const box = await this.boxRepository.findOne({
      where: { id: boxId, owner }
    })
    if (!owner || !box)
      throw new NotFoundException(`Cannot find box n°${boxId}`)

    const entry = await this.entryRepository.findOne({
      where: { id: entryId },
      relations: ['expressions', 'expressions.strates']
    })

    if (!entry) throw new NotFoundException(`cannot find entry n°${entryId}`)

    const expression = this.expressionRepository.create({
      ...input,
      entry
    })

    const exp = await this.expressionRepository.save(expression)
    return exp
  }

  async updateExpression(
    entryId: string,
    expressionId: string,
    userId: string,
    input: UpdateExpressionDTO
  ) {
    const entry = await this.entryRepository.findOne({
      where: { id: entryId },
      relations: ['box', 'box.owner']
    })

    if (!entry || entry.box.owner.id !== userId) {
      throw new NotFoundException(`Cannot find entry n°${entryId}`)
    }

    const expression = await this.expressionRepository.findOne({
      where: { id: expressionId },
      relations: ['entry']
    })

    if (!expression || expression.entry.id !== entry.id) {
      throw new NotFoundException(`Cannot find expression n°${expressionId}`)
    }

    const updatedExpression = { ...expression, ...input }
    await this.expressionRepository.save(updatedExpression)
    return updatedExpression
  }

  async deleteExpression(
    entryId: string,
    expressionId: string,
    userId: string
  ): Promise<ExpressionEntity> {
    const entry = await this.entryRepository.findOne({
      where: { id: entryId },
      relations: ['box', 'box.owner']
    })
    if (!entry || entry.box.owner.id !== userId)
      throw new NotFoundException(`Cannot find entry n°${entryId}`)

    const expression = await this.expressionRepository.findOne({
      where: { id: expressionId },
      relations: ['entry']
    })

    if (!expression || expression.entry.id !== entry.id) {
      throw new NotFoundException(`Cannot find expression n°${expressionId}`)
    }
    await this.expressionRepository.delete(expression.id)
    return expression
  }

  private async get(model: string, id: string, relations?: string[]) {
    try {
      const repo = this[`${model.toLowerCase()}Repository`]
      if (!repo) throw new Error(`Repo for ${model} does not exist`)
      return await repo.findOneOrFail({
        where: { id },
        relations
      })
    } catch (e) {
      throw new NotFoundException(model + ' not found')
    }
  }
}
