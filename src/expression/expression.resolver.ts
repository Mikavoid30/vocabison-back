import { Resolver, Query, Context, Args, Mutation } from '@nestjs/graphql'
import { SetMetadata, UseGuards } from '@nestjs/common'
import { Authorized } from '../common/guards/authorized.guard'
import { roles } from '../common/enums/roles.enum'
import { ExpressionService } from './expression.service'
import { CreateExpressionDTO } from './dto/create-expression.dto'
import { UpdateExpressionDTO } from './dto/update-expression.dto'
import { ExpressionEntity } from './expression.entity'

@Resolver('expression')
export class ExpressionResolver {
  constructor(private expressionService: ExpressionService) {}

  @Query('expressions')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  expressions(
    @Args('entryId') entryId: string,
    @Args('n') n: number /** @todo pagination */,
    @Args('p') p: number,
    @Context() ctx: any
  ): Promise<ExpressionEntity[]> {
    return this.expressionService.getAllExpressionFromEntry(
      entryId,
      ctx.req.session.userId,
      n,
      p
    )
  }

  @Query('expression')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  expression(
    @Args('entryId') entryId: string,
    @Args('expressionId') expressionId: string,
    @Context() ctx: any
  ): Promise<ExpressionEntity> {
    return this.expressionService.findExpression(
      entryId,
      expressionId,
      ctx.req.session.userId
    )
  }

  @Mutation('createExpression')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  createExpression(
    @Args('boxId') boxId: string,
    @Args('entryId') entryId: string,
    @Args('createExpressionInput') input: CreateExpressionDTO,
    @Context() ctx: any
  ): Promise<ExpressionEntity> {
    return this.expressionService.createExpression(
      boxId,
      entryId,
      ctx.req.session.userId,
      input
    )
  }

  @Mutation('updateExpression')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  updateExpression(
    // @Args('boxId') boxId: string,
    @Args('entryId') entryId: string,
    @Args('expressionId') expressionId: string,
    @Args('updateExpressionInput') input: UpdateExpressionDTO,
    @Context() ctx: any
  ): Promise<ExpressionEntity> {
    return this.expressionService.updateExpression(
      // boxId,
      entryId,
      expressionId,
      ctx.req.session.userId,
      input
    )
  }

  @Mutation('deleteExpression')
  @SetMetadata('roles', [roles.USER])
  @UseGuards(Authorized)
  deleteExpression(
    @Args('entryId') entryId: string,
    @Args('expressionId') expressionId: string,
    @Context() ctx: any
  ): Promise<ExpressionEntity> {
    return this.expressionService.deleteExpression(
      entryId,
      expressionId,
      ctx.req.session.userId
    )
  }
}
