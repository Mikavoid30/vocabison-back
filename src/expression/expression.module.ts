import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { EntryEntity } from '../entry/entry.entity'
import { ExpressionEntity } from './expression.entity'
import { EntryService } from '../entry/entry.service'
import { ExpressionService } from './expression.service'
import { BoxEntity } from '../box/box.entity'
import { UserEntity } from '../user/user.entity'
import { ExpressionResolver } from './expression.resolver'
import { UserService } from '../user/user.service'
import { DateScalar } from '../common/graphql/scalars/date.scalar'
import { StrateEntity } from '../strate/strate.entity'
import { StrateService } from '../strate/strate.service'

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ExpressionEntity,
      EntryEntity,
      BoxEntity,
      UserEntity,
      StrateEntity
    ])
  ],
  controllers: [],
  providers: [
    EntryService,
    StrateService,
    ExpressionService,
    DateScalar,
    UserService,
    ExpressionResolver
  ],
  exports: [ExpressionService]
})
export class ExpressionModule {}
