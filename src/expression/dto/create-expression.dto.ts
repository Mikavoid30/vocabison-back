import { IsString, MinLength, MaxLength, IsOptional } from 'class-validator'
import { CreateExpressionInput } from '../../graphql.schema'

export class CreateExpressionDTO extends CreateExpressionInput {
  @IsString()
  @MinLength(2)
  readonly text: string

  @IsString()
  @MinLength(2)
  @MaxLength(2)
  readonly lang: string
}
