import { IsString, MinLength, MaxLength, IsOptional } from 'class-validator'
import { UpdateExpressionInput } from '../../graphql.schema'

export class UpdateExpressionDTO extends UpdateExpressionInput {
  @IsString()
  @MinLength(2)
  @IsOptional()
  readonly text: string

  @IsString()
  @MinLength(2)
  @MaxLength(2)
  @IsOptional()
  readonly lang: string

  @IsString({ each: true })
  @IsOptional()
  readonly definitions: string[]

  @IsString({ each: true })
  @IsOptional()
  readonly synonyms: string[]

  @IsString({ each: true })
  @IsOptional()
  readonly examples: string[]
}
