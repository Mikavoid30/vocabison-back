import faker from 'faker'
import { RegisterUserDTO } from '../../src/user/dto/register-user.dto'
import { ConfirmUserDTO } from '../../src/user/dto/confirm-user.dto'

export class Helper {
  constructor(private server: any) {}

  async runGql(gql: string, user?: any): Promise<any> {
    return this.server.post('/graphql').send({ query: gql })
  }

  runRegister(testUser: RegisterUserDTO): any {
    return this.runGql(`
          mutation {
            register(
              registerInput: {
                email: "${testUser.email}"
                password: "${testUser.password}"
                native_language: "${testUser.native_language}"
              }
            ) {
              id
              email
              native_language
              created
            }
          }
        `)
  }

  runLogin(testUser: Partial<RegisterUserDTO>): any {
    return this.runGql(`
          mutation {
            login(
              loginInput: {
                email: "${testUser.email}"
                password: "${testUser.password}"
              }
            ) {
              id
              email
              native_language
              created
            }
          }
        `)
  }

  runConfirm(testUser: ConfirmUserDTO): any {
    return this.runGql(`
          mutation {
            confirm (token: "${testUser.token}")
          }
        `)
  }

  getFakeUserToRegister(): Promise<RegisterUserDTO> {
    return Promise.resolve({
      email: faker.internet.email(),
      native_language: 'FR',
      password: 'password'
    })
  }
}

export const SESSION_OPTIONS = {
  name: 'qid',
  secret: 'secret*',
  resave: false,
  saveUninitialized: false,
  cookie: {
    httpOnly: true,
    secure: false,
    maxAge: 1000 * 60 * 60
  }
}
