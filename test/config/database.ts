export default {
  type: 'postgres',
  host: process.env.TYPEORM_HOST,
  port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
  username: 'postgres',
  password: 'password',
  database: 'testdb',
  logging: false,
  dropSchema: true,
  synchronize: true,
  migrationsRun: false,
  entities: ['src/**/**.entity.ts', 'dist/**/**.entity.js']
}
